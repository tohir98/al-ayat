<div class="row-fluid">
	<div class="page-header">
		<div class="pull-left">
			<a href="home.php?page=other-income" class="btn btn-warning"><i class="icon-arrow-left"></i> &nbsp;Back</a>
			<br />
			<h1>New Record</h1>
		</div>
		<div class="clearfix"></div>
	</div>
	
	<!-- Breadcrumb -->
	<div class="breadcrumbs">
		<ul>
			<li>
				<a href="home.php">Dashboard</a>
				<i class="icon-angle-right"></i>
			</li>
			<li>
				<a href="home.php?page=financial-sheet">Financial Sheet</a>
				<i class="icon-angle-right"></i>
			</li>
			<li>
				<a href="#">Add Record</a>
			</li>
		</ul>
	</div>
	<!-- End Breadcrumb -->
	

	<div class="row-fluid">
		<div class="span12">
			<div class="box box-bordered">
				<div class="box-title">
					<h3>
						<i class="icon-ok"></i>
						Add Financial 
					</h3>
				</div>
				<div class="box-content">
					<form id="frmadd" action="home.php?page=financial-sheet&subpage=process_sheet&action=add" method="POST" class='form-horizontal form-validate form-striped'>
						<div class="control-group">
							<label for="textfield" class="control-label">Member</label>
							<div class="controls">
								<?php $Rusr = mysql_query("SELECT * FROM tbl_customer WHERE location_id = '".$_SESSION[location_id]."' ");?>
								<select name="customer_id" id="customer_id" class="select2-me input-large">
									<option value="0">Select member</option>
									<?php while($Rowsusr=mysql_fetch_array($Rusr)){ ?>
									<option value="<?php echo $Rowsusr['customer_id']; ?>"><?php echo ucfirst(trim($Rowsusr[1]))." ".ucfirst(trim($Rowsusr[2]));?></option>
									<?php } ?>
								</select>
                            </div>
						</div>
						<div class="control-group">
							<label for="textfield" class="control-label">Devt. Levy</label>
							<div class="controls">
								<input type="text" name="dl" id="dl" class="" value="0">
							</div>
						</div>
						<div class="control-group">
							<label for="emailfield" class="control-label">Shares</label>
							<div class="controls">
								<input type="text" name="sh" id="sh" class=""  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="pwfield" class="control-label">Savings</label>
							<div class="controls">
								<input type="text" name="sa" id="sa" class="compute"  value="0"/>
							</div>
						</div>
						<div class="control-group">
							<label for="confirmfield" class="control-label">Insurance</label>
							<div class="controls">
								<input type="text" name="i" id="i" class="compute" value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="policy" class="control-label">Ileya</label>
							<div class="controls">
								<input type="text" name="il" id="il" class="compute"  value="0"/>
							</div>
						</div>
						<div class="control-group">
							<label for="COT" class="control-label">SEC</label>
							<div class="controls">
								<input type="text" name="sec" id="sec" class="compute"  value="0"/>
							</div>
						</div>
						<div class="control-group">
							<label for="Zakat" class="control-label">OTS</label>
							<div class="controls">
								<input type="text" id="ots" name="ots" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label alert-danger">Loan</label>
							<div class="controls">
								<input type="text" id="loan" name="loan" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">LRP</label>
							<div class="controls">
								<input type="text" id="lrp" name="lrp" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label alert-danger">SDS</label>
							<div class="controls">
								<input type="text" id="sds" name="sds" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">SDSR</label>
							<div class="controls">
								<input type="text" id="sdsr" name="sdsr" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">HAJJ</label>
							<div class="controls">
								<input type="text" id="hajj" name="hajj" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">Total</label>
							<div class="controls">
								<div id="total" name="total" class="" ></div>
							</div>
						</div>
						
						<div class="form-actions">
							<input type="button" id="btn_save" class="btn btn-primary" value="Save">
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
<script>
	$(document).ready(function() {

 
    
	$('#btn_save').click(function() {
            //show_loader();
            $('#btn_save').attr('disabled', true);
			$('#frmadd').submit();
			return false;
		
	});
	
	$("input").delegate(".compute", "click", function( event ) {
		alert('hi');
	  var total = parseInt($('#hajj').val()) + parseInt($('#sdsr').val()) + parseInt($('#lrp').val()) + parseInt($('#loan').val()) ;
            $('#total').html(total);
	});	
	
	$('.compute').keyup(function() {
	
			 var total = parseInt($('#hajj').val()) 
			 			+ parseInt($('#sdsr').val()) 
						+ parseInt($('#lrp').val()) 
						+ parseInt($('#loan').val()) 
						+ parseInt($('#ots').val()) 
						+ parseInt($('#sec').val()) 
						+ parseInt($('#il').val()) 
						+ parseInt($('#i').val()) 
						+ parseInt($('#sa').val()) 
						+ parseInt($('#sh').val()) 
						+ parseInt($('#dl').val()) 
						+ parseInt($('#sds').val()) ;
						
            $('#total').html('<b>'+total+'</b>');
			return false;
		
	});	
});
</script>
		
		