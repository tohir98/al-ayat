<?php
	session_start();
	include("../umfcon.inc");
	include("../methods.php");
	if (isset($_GET['id'])){
	$id = $_GET['id'];
	$Qchker = "SELECT fs.*, fs.dl+fs.sh+fs.sa+fs.i+fs.il+fs.sec+fs.ots+fs.loan+fs.lrp+fs.sds+fs.sdsr+fs.hajj total , c.first_name, c.last_name
				FROM tbl_financial_sheet fs
				INNER JOIN tbl_customer c ON fs.customer_id = c.customer_id WHERE financial_sheet_id = '$id'";
	$Rchker = mysql_query($Qchker);
	if (mysql_num_rows($Rchker) > 0){
	$RowRchker = mysql_fetch_array($Rchker);
	
	$financial_sheet_id   = $RowRchker['financial_sheet_id'];
	$first_name = $RowRchker['first_name'];
	$last_name = $RowRchker['last_name'];
	$dl = $RowRchker['dl'];
	$sh = $RowRchker['sh'];
	$sa = $RowRchker['sa'];
	$i = $RowRchker['i'];
	$il = $RowRchker['il'];
	$sec = $RowRchker['sec'];
	$ots = $RowRchker['ots'];
	$loan = $RowRchker['loan'];
	$lrp = $RowRchker['lrp'];
	$sds = $RowRchker['sds'];
	$sdsr = $RowRchker['sdsr'];
	$hajj = $RowRchker['hajj'];
	$month = $RowRchker['month'];
	$year = $RowRchker['year'];
	$edit = "1";
	}}
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 id="myModalLabel">Financial Sheet [<?php echo $first_name .' '. $last_name; ?>]&nbsp;&nbsp;&nbsp;<div class="label"> <?php echo $months[$month]; ?></div>
	<div class="label"><?php echo $year ?></div> </h4>
</div>
<div class="modal-body padless">

	<form class="form-horizontal form-striped" id="frm_edit_user_type" method="post" action="home.php?page=users&subpage=process_user&action=edit_user">
	<table class="table table-striped">
	
		<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Dev. Levy</label>
                            <div class="controls">
							<label for="select" class="control-label"><?php if (isset($dl)){ echo number_format($dl,2);}?></label>
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Shares</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($sh)){ echo number_format($sh,2);}?></label>
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Savings</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($sa)){ echo number_format($sa,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Insurrance</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($i)){ echo number_format($i,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Ileya</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($il)){ echo number_format($il,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">OTS</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($ots)){ echo number_format($ots,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">SEC</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($sec)){ echo number_format($sec,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Loan</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($loan)){ echo number_format($loan,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Loan Repayment</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($lrp)){ echo number_format($lrp,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">SDS</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($sds)){ echo number_format($sds,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">SDS Repayment</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($sdsr)){ echo number_format($sdsr,2);}?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">HAJJ</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($hajj)){ echo number_format($hajj,2);}?></label>
                            </div>
                        </div>

		
	</table>
	<form>
</div>

<div class="modal-footer">
	<a href="financial-sheet/edit_record.php?id=<?php echo $financial_sheet_id; ?>" class="btn btn-success" data-dismiss="modal" aria-hidden="true">Edit</a>
	<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
</div>



<script>


	
	$("#btn_edit_user").click(function() {
		$("#frm_edit_user_type").submit();
		$("#btn_edit_user_type").prop('disabled', true);
    });  

    $('input[type=submit]').click(function(e){
		$('form').submit();
	    $(this).prop('disabled', true);
	});

</script>