<?php if ($_SESSION['error'] != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $_SESSION['error'] ; ?>
</div>
<?php $_SESSION['error'] = '' ; } 

if ($_SESSION['success'] != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $_SESSION['success']; ?>
</div>
<?php $_SESSION['success'] = ''; }  ?>