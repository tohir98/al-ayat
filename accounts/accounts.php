<?php 
if ($_SESSION['logged'] != true){
		$msg = base64_encode("Welcome, Please Login!...");
		header("Location: ../login.php?msg=$msg");
	}
	?>

<?php include './_notification.php'; ?>

<!-- starting tab -->
<div class="box">
    
    <div class="box-content nopadding">
        <p>
            <ul class="nav nav-tabs" id="myTab">
                <li class="active">
                    <a href="#active" data-toggle="tab">Active</a>
                </li>
                <li>
                    <a href="#inactive" data-toggle="tab">In-Active</a>
                </li>
            </ul>
        </p>
                                        
        <div class="tab-content"> 
            <div class="tab-pane active" id="active">
			<div id="err_div" style="display:none" class="alert alert-error">Pls, select account(s) to deactivate </div>     
			<div class="row-fluid">
				<div class="span12">
					<div class="box box-bordered">
					<div class="box-title">
							<div class="btn-group">
								<a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">Bulk Action <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="#" class="disable">Deactivate</a></li>
								</ul>
							</div>
							<div class="pull-right" style="margin-right:10px"> 
								<a href="home.php?page=accounts&subpage=new_account" class="btn btn-primary">
								<i class="icon-user"></i> New Member
								</a>
							</div>
						</div>
				<div class="box-content">
				<form name="frm_enrolled" id="frm_enrolled" method="POST" action="<?php echo base_url() ?>page=accounts&subpage=process_account&action=deactivate">
					
					<table class="table table-user table-hover table-nomargin table-bordered dataTable table-condensed ">
						<thead>
							 <tr>
								<th><input name="checkbox" type="checkbox" onClick="toggle(this)" /></th>
								<th>AccountNo</th>
								<th>Customer Name</th>
								<th class='hidden-1024'>Phone</th>
								<th class='hidden-480'>Gender</th>
								<th class='hidden-480' nowrap>Action</th>
							</tr>
						</thead>
						<tbody>
						 <?php 
							$Rqry = ExecuteSQLQuery("SELECT c.* , g.gender
										FROM tbl_customer c
										LEFT JOIN tbl_gender g ON c.gender_id = g.gender_id
										WHERE c.deleted = 0 and c.location_id = '".$_SESSION['location_id']."' ");
										
						  $counter = 1;
						  while ($rowRqry = mysqli_fetch_array($Rqry)){  ?>
							
							   <tr>
									<td width="50"><input class="act_chk" type="checkbox" name="pin[]" value="<?php echo $rowRqry["customer_id"] ?>" /></td>
									<td> <?php echo strtoupper($rowRqry["acct_no"]); ?>  </td>
									<td> <?php echo strtoupper($rowRqry["first_name"]) . ' ' . strtoupper($rowRqry["last_name"]); ?>  </td>
									<td> <?php echo strtoupper($rowRqry["phone"]); ?> </td>
									<td><span class="muted"><?php echo strtoupper($rowRqry["gender"]); ?></span></td>
									<td>
										<div class="btn-group">
											<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="<?php echo base_url() ?>page=accounts&subpage=customer_profile&id=<?php echo $rowRqry["customer_id"] ?>">View Records </a></li>
												<li><a href="home.php?page=accounts&subpage=process_account&action=delete&id=<?php echo $rowRqry["customer_id"]?>" class="modal_confirm_customer">Deactivate </a></li>
												<li><a href="">Edit User </a></li>
													
											</ul>
										</div>
									</td>
							</tr>
								
							<?php } ?>
						</tbody>
					</table>
					
				</form>
					</div>
					</div>
				</div>
			</div>
            </div><!-- End Div Active -->
            
            
            
            <div class="tab-pane" id="inactive">
                <form name="frm_inactive" id="frm_inactive" method="POST" action="<?php echo base_url() ?>page=accounts&subpage=process_account&action=activate">
				<div id="err_div1" style="display:none" class="alert-error">Pls, select account to activate </div>     
                    <div class="box-title">
                        <div class="btn-group">
                            <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">Bulk Action <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="enable">Activate</a></li>
                            </ul>
                        </div>
                    </div>
                    <table class="table table-user table-hover table-nomargin table-bordered dataTable ">
                        <thead>
                             <tr>
                    			<th><input name="checkbox" type="checkbox" onClick="toggle1(this)" /></th>
                                <th>AccountNo</th>
                                <th>FirstName</th>
                                <th>LastName</th>
                                <th class='hidden-1024'>Phone</th>
                                <th class='hidden-480'>Gender</th>
                                <th class='hidden-480' nowrap>Action</th>
                            </tr>
                        </thead>
                                
                        <tbody>
						
                         <?php 
						$Rqry = ExecuteSQLQuery("SELECT c.* , g.gender
										FROM tbl_customer c
										LEFT JOIN tbl_gender g ON c.gender_id = g.gender_id
										WHERE c.deleted = 1 and c.location_id = '".$_SESSION['location_id']."' ");
						  $counter = 1;
						  while ($rowRqry = mysqli_fetch_array($Rqry)){  ?>
                                    
						<tr>
							<td width="50"><input class="act_chk_d" type="checkbox" name="pin1[]" value="<?php echo $rowRqry["customer_id"] ?>" /></td>
							<td> <?php echo strtoupper($rowRqry["acct_no"]); ?>  </td>
							<td> <?php echo strtoupper($rowRqry["first_name"]); ?>  </td>
							<td> <?php echo strtoupper($rowRqry["last_name"]); ?> </td>
							<td> <?php echo strtoupper($rowRqry["phone"]); ?> </td>
							<td><span class="muted"><?php echo strtoupper($rowRqry["gender"]); ?></span></td>
							<td>
							<div class="btn-group">
								<a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
								<ul class="dropdown-menu">
										<li><a href="">View Records </a></li>
										
										<li><a href="home.php?page=accounts&subpage=process_account&action=delete&id=<?php echo $rowRqry["customer_id"]?>" class="modal_confirm_customer">Delete Account </a></li>
										<li><a href="">Edit User </a></li>
										
								</ul>
							</div>
							</td>
						</tr>
                                        
                        <?php } ?>
                    </tbody>
                </table>
            </form>
            </div><!-- End Div not active -->

        </form></div>
    </div>    
</div>

<!-- Remove customer Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-body">
    	<p>Are you sure you want to delete customer?</p>
  </div>
  <div class="modal-footer">
    	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="#" class="btn btn-danger closeme">Yes, delete customer account</a>
  </div>
</div>
<!-- End Remove customer Modal -->

<div id="modal-enable" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>Are you sure you want to activate selected account(s)?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
		<button data-dismiss="modal" id="enable" class="btn btn-danger closeme">Yes, activate account(s)</button>
  </div>
</div>

<div id="modal-disable" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-body">
    <p>Are you sure you want to deactivate selected account(s)?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
		<button data-dismiss="modal" id="disable" class="btn btn-danger closeme">Yes, deactivate account(s)</button>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    
            
	$('#id_statuses').change(function() {
		$('#sel').submit();
	});
	
	    
    $('.modal_confirm_customer').click(function(eve){
        
        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();
        
        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
        
    });
    
    $('.enable').click(function(eve){
		var checked_count = $(".act_chk_d:checked").length;
		if(checked_count <= 0) {
			$('#err_div1').show();
			return false;
		} 
        eve.preventDefault();
        $('#modal-enable').modal('show').fadeIn();
        return false;
    });
    
    $('.disable').click(function(eve){
        eve.preventDefault();
		var checked_count = $(".act_chk:checked").length;
		if(checked_count <= 0) {
			$('#err_div').show();
			return false;
		} 
        $('#modal-disable').modal('show').fadeIn();
        return false;
    });
    
    $('#enable').click(function(eve){ 
        $("form#frm_inactive").submit();
    });
    
    $('#disable').click(function(eve){ 
        $("form#frm_enrolled").submit();
    });
    
    
    
    $('.ajaxlink_user_type').click(function(eve){
        
        eve.preventDefault();
        $('#contribution').modal('show');
        $('#contribution').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#contribution').html('');
          $('#contribution').html(html).show();

          
        });
        });
        
       
});



 $(function () {
        $('#myTab a:first').tab('show');
    })
    
    function toggle(source) {
      checkboxes = document.getElementsByName('pin[]');
      for(var i in checkboxes)
        checkboxes[i].checked = source.checked;
    }
    
    function toggle1(source) {
      checkboxes = document.getElementsByName('pin1[]');
      for(var i in checkboxes)
        checkboxes[i].checked = source.checked;
    }
    
</script>