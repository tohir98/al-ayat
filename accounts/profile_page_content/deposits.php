<div class="box box-color box-bordered primary employee_info_box" id="company_profile_container" style="display:block;">
	<div class="box-title">
		<h3>
			Company Profile
		</h3>
	</div>
	<div class="box-content">

		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Profile</h4>
				</li>

				<li class="dropdown pull-right">
					<a data-toggle="dropdown" class="dropdown-toggle" href="#">Action <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo site_url('tbadmin/employers/edit_profile/'.$profile[0]->id_string) ?>">Edit Now</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<p></p>
		<table style="cellpadding:5px">
			<tr>
				<td><strong>About Us:</strong></td>
				<td><?php if(isset($profile[0]->about_us)) { echo $profile[0]->about_us; } ?></td>
			</tr>
			<tr>
				<td><strong>Banner Text:</strong></td>
				<td><?php if(isset($profile[0]->banner_text)) { echo $profile[0]->banner_text; } ?></td>
			</tr>
			<tr>
				<td><strong>Description:</strong></td>
				<td><?php if(isset($profile[0]->description)) { echo $profile[0]->description; } ?></td>
			</tr>
			<tr>
				<td><strong>Website:</strong></td>
				<td><?php if(isset($profile[0]->website)) { echo $profile[0]->website; } ?></td>
			</tr>
			<tr>
				<td><strong>Number of Employees:</strong></td>
				<td><?php if(isset($profile[0]->range_of_employees)) { echo $profile[0]->range_of_employees; } ?></td>
			</tr>
			<tr>
				<td><strong>Date Registered:</strong></td>
				<td><?php if(isset($profile[0]->date_registered)) { echo $profile[0]->date_registered; } ?></td>
			</tr>
		</table>
	</div>
</div>
