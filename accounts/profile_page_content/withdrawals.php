<div class="box box-color box-bordered primary employee_info_box" id="modules_container" style="display:none;">
	<div class="box-title">
		<h3>
			Modules
		</h3>
	</div>
	<div class="box-content">

		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Assigned Modules</h4>
				</li>
			</ul>
		</div>
		<p></p>
		
		<?php $i = array(); 
			if(empty($modules)) { echo 'No assigned modules.'; } else {
				foreach ($modules as $module) { 
					if(!in_array($module->id_module, $i)) {
						$i[] = $module->id_module;
		?>
		<div class="check-line">
			<input type="checkbox" class='icheck-me remove_module' data-skin="square" data-color="blue" data-id_module="<?php echo $module->id_module ?>" data-id_string="<?php echo $module->id_string ?>" checked> <label class='inline' for="c6"> <?php echo $module->subject ?></label>
		</div>
		<?php } } } ?>
		
		<p></p>
		<div style="border-bottom:1px solid #CCCCCC;">
			<ul class="nav nav-pills" style="margin-bottom:5px;">
				<li class="active">
					<h4>Modules</h4>
				</li>
			</ul>
		</div>
		<p></p>
		
		<?php if(empty($def_modules)) { echo 'No more modules to be assigned'; } else {
			foreach ($def_modules as $d) {
				if(!in_array($d->id_module, $i)){
		?>
		<div class="check-line">
			<input type="checkbox" class='icheck-me add_module' data-skin="square" data-color="blue" data-id_module="<?php echo $d->id_module ?>" data-id_string="<?php echo $id_string ?>"> <label class='inline' for="c6"> <?php echo $d->subject ?></label>
		</div>
		<?php } } } ?>
	</div>
</div>

<!-- Modal confirmation box for delete modules -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="myModal_1" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Assigned Modules</h4>
      </div>
      <div class="modal-body">
        <p>Do you want to delete module from company's list?</p>
      </div>
      <div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true" id="cancel_button">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_button">Yes</button>
	  </div>
</div>

<!-- Modal confirmation box for adding modules -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal hide fade" id="myModal_2" style="display: none;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Assigned Modules</h4>
      </div>
      <div class="modal-body">
        <p>Do you want to add module to company's list?</p>
      </div>
      <div class="modal-footer">
		<button data-dismiss="modal" class="btn" aria-hidden="true" id="cancel_button_module">Cancel</button>
		<button data-dismiss="modal" class="btn btn-primary" id="submit_dialog_button_add">Yes</button>
	  </div>
</div>

<script>
var data;
var url_1 = "<?php echo site_url('tbadmin/employers/remove_module/'); ?>";
var url_2 = "<?php echo site_url('tbadmin/employers/add_module/'); ?>";
var id_string;

$('.remove_module').click(function(e){
	e.preventDefault();
	data = $(this).data('id_module');
	id_string = $(this).data('id_string');
	$('#myModal_1').modal('show');
});

$('#submit_dialog_button').click(function(e){
	$('#myModal_1').modal('hide');
	window.location.href = url_1+'/'+data+'/'+id_string;
});

$('.add_module').click(function(e){
	e.preventDefault();
	data = $(this).data('id_module');
	id_string = $(this).data('id_string');
	$('#myModal_2').modal('show');
});

$('#submit_dialog_button_add').click(function(e){
	$('#myModal_2').modal('hide');
	window.location.href = url_2+'/'+data+'/'+id_string;
});

</script>
