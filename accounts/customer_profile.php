<?php
	session_start();
	if ($_SESSION['logged'] != true){
		$msg = base64_encode("Welcome, Please Login!...");
		header("Location: ../login.php?msg=$msg");
	}
	
	include("../umfcon.inc");
	if (isset($_GET['id'])){
		$id = $_GET['id'];
		$Qchker = "SELECT * FROM tbl_customer WHERE customer_id= '$id'";
		$Rchker = mysql_query($Qchker);
		if (mysql_num_rows($Rchker) > 0){
			$RowRchker = mysql_fetch_array($Rchker);
		
			$customer_id  = $RowRchker['customer_id'];
			$first_name = $RowRchker['first_name'];
			$last_name = $RowRchker['last_name'];
			$edit = "1";
		}
	}
?>
<!-- Display error/success message -->
<br/>
<?php if ($_SESSION['error'] != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $_SESSION['error'] ; ?>
</div>
<?php $_SESSION['error'] = '' ; } 

if ($_SESSION['success'] != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $_SESSION['success']; ?>
</div>
<?php $_SESSION['success'] = ''; }  ?>

<div class="box">
	<div class="box-content">
			<div class="row-fluid">
				<div class="span12">
					<a class="btn btn-warning" href="<?php echo base_url() ?>page=accounts"><i class="icon-angle-left"></i> Back</a>
					<a class="btn btn-primary" href="#"><i class="icon-print"></i> Print</a>
					<a class="btn btn-primary" href="#"><i class="icon-file"></i> PDF</a>
				</div>
			</div>
			<div class="row-fluid" style="margin-top:20px;">
				<div class="span12">
					<h4><?php echo ucfirst($first_name) . ' ' . ucfirst($last_name) ?></h4>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span2">
					<div class="box">
						<div class="box-title" style="border:1px solid #CCCCCC;height:210px;padding:0;">
							<div class="row-fluid" style="height:180px;overflow:hidden">
								
							</div>
							<div class="row-fluid">
								<div class="btn-group span12">
							<a class="btn btn-primary dropdown-toggle span12" data-toggle="dropdown" href="#"><span class="caret"></span> Edit </a>
							<ul class="dropdown-menu dropdown-primary">
								<li>
									<a href="#">Edit Now</a>
								</li>
							</ul>
							</div>
							</div>	
						</div>
						<div class="_box-content">
							
							<div class="span12">
								<p></p>
								<ul class="nav nav-tabs nav-stacked left_menu">
									<li class="active"><a href="accounts/profile_page_content/profile.php?id=<?php echo $_GET['id'] ?>" target="mainFrame" >Profile</a></li>
									<li><a href="accounts/profile_page_content/deposits.php?id=<?php echo $_GET['id'] ?>" target="mainFrame" >Deposits</a></li>
									<li><a href="accounts/profile_page_content/withdrawals.php?id=<?php echo $_GET['id'] ?>" target="mainFrame">Withdrawals</a></li>
								</ul>
								<p></p>
							</div>
														
						</div>
					</div>
					
				</div>
				<div class="span10">
					<iframe name="mainFrame" id="mainFrame" height="600px" style="width:100%;" frameborder=0>
						
					</iframe>
				</div>
			</div>
		
	</div>
</div>

<script type="text/javascript">

$(document).ready(function() {

$('.left_menu_button').click(function() {
	
	$('.left_menu_button').parent().removeClass('active');
	$(this).parent().addClass('active');
	
	$('.employee_info_box').hide();
	
	var container_id = $(this).attr('data-container');
	$('#' + container_id).show();
	
	return false;
});	
});	


</script>