<div class="page-header">
    <div class="pull-left">
        <a href="home.php?page=accounts" class="btn btn-warning"><i class="icon-arrow-left"></i> &nbsp;Back</a>
        <br />
        <h1>New Member</h1>
    </div>
    <div class="clearfix"></div>
</div>
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php?page=accounts">Accounts</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Create New Account</a>
            <i class="icon-angle-right"></i>
        </li>
    </ul>
</div>
<!-- display errors here -->

<div class="row-fluid">
    <div class="span12">
        <div class="box">
            <div class="box-title">
                <h3>
                    <i class="icon-ok"></i>
                    Membership Registration
                </h3>
            </div>
            <div class="box-content">
                <form action="home.php?page=accounts&subpage=process_account&action=add_account" method="POST" class='form-horizontal form-validate' id="frmnew_account">
                    <div class="control-group">
                        <label for="textfield" class="control-label">FirstName *</label>
                        <div class="controls">
                            <input type="text" name="first_name" id="first_name" class="input-xlarge" data-rule-required="true" data-rule-minlength="2">
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="emailfield" class="control-label">Lastname *</label>
                        <div class="controls">
                            <input type="text" name="last_name" id="last_name" class="input-xlarge" data-rule-required="true" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="pwfield" class="control-label">Gender *</label>
                        <div class="controls">
                            <?php
                            if (isset($gender_id)) {
                                $Qgenda2 = "select * from tbl_gender where gender_id != '$gender_id'";
                                $Rgenda2 = mysqli_query($dbhandle, $Qgenda2);
                                $Qgenda1 = "select * from tbl_gender where gender_id = '$gender_id'";
                                $Rgenda1 = mysqli_query($dbhandle, $Qgenda1);
                                $rowgenda1 = mysqli_fetch_array($Rgenda1);
                            } else {
                                $Qgenda2 = "select * from tbl_gender order by gender ASC";
                                $Rgenda2 = mysqli_query($dbhandle, $Qgenda2);
                            }
                            ?>
                            <select name="gender_id" class="select2-me input-large" id="gender_id">
                                <option value="0">Select gender</option>
                                <?php if (isset($gender_id)) { ?>
                                    <option  selected="selected" value="<?php echo $rowgenda1['gender_id']; ?>"><?php echo $rowgenda1['gender']; ?></option>
                                <?php } ?>
                                <?php while ($Rowsgenda2 = mysqli_fetch_array($Rgenda2)) { ?>
                                    <option value="<?php echo $Rowsgenda2['gender_id']; ?>"><?php echo $Rowsgenda2['gender']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="confirmfield" class="control-label">Marital Status *</label>
                        <div class="controls">
                            <?php
                            if (isset($marital_status_id)) {
                                $Qmarital2 = "select * from tbl_marital_status where marital_status_id != '$marital_status_id'";
                                $Rmarital2 = mysqli_query($dbhandle, $Qmarital2);
                                $Qmarital1 = "select * from tbl_marital_status where marital_status_id = '$marital_status_id'";
                                $Rmarital1 = mysqli_query($dbhandle, $Qmarital1);
                                $rowmarital1 = mysqli_fetch_array($Rmarital1);
                            } else {
                                $Qmarital2 = "select * from tbl_marital_status order by marital_status ASC";
                                $Rmarital2 = mysqli_query($dbhandle, $Qmarital2);
                            }
                            ?>
                            <select name="marital_status_id" class="select2-me input-large" id="marital_status_id" data-rule-required="true">
                                <option value="">Select marital status</option>
                                <?php if (isset($marital_status_id)) { ?>
                                    <option  selected="selected" value="<?php echo $rowmarital1['marital_status_id']; ?>"><?php echo $rowmarital1['marital_status']; ?></option>
                                <?php } ?>
                                <?php while ($Rowsmarital2 = mysqli_fetch_array($Rmarital2)) { ?>
                                    <option value="<?php echo $Rowsmarital2['marital_status_id']; ?>"><?php echo $Rowsmarital2['marital_status']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="policy" class="control-label">Street/City *</label>
                        <div class="controls">
                            <input type="text" name="address" id="address" value="" placeholder="Street" data-rule-required="true" data-rule-minlength="2"/> /
                            <input type="text" name="city" id="city" value="" placeholder="City" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="policy" class="control-label">State/LGA *</label>
                        <div class="controls">
                            <div style="float:left;">
                                <?php
                                if (isset($state_id)) {
                                    $Rloc2 = mysqli_query($dbhandle, "SELECT * FROM tbl_states WHERE StateID != '$state_id'");
                                    $Rloc1 = mysqli_query($dbhandle, "SELECT * FROM tbl_states WHERE StateID = '$state_id'");
                                    $rowloc1 = mysqli_fetch_array($Rloc1);
                                } else {
                                    $Rloc2 = mysqli_query($dbhandle, "SELECT * FROM tbl_states");
                                }
                                ?>

                                <select name="state_id" id="state_id" class="select2-me input-large" onChange="getLga(this.value)">
                                    <option value="0">Select state</option>
                                    <?php if (isset($state_id)) { ?>
                                        <option  selected="selected" value="<?php echo $rowloc1['StateID']; ?>"><?php echo $rowloc1['StateName']; ?></option>
                                    <?php } ?>
                                    <?php while ($Rowsloc2 = mysqli_fetch_array($Rloc2)) { ?>
                                        <option value="<?php echo $Rowsloc2['StateID']; ?>"><?php echo $Rowsloc2['StateName']; ?></option>
                                    <?php } ?>
                                </select>
                                &nbsp;/&nbsp;
                            </div>
                            <div id="lgadiv">
                                <select name="local_govt_id" id="local_govt_id">
                                    <option value="0">Select State First</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="policy" class="control-label">Phone *</label>
                        <div class="controls">
                            <input value="" type="text" id="phone" name="phone" class="" data-rule-required="true" data-rule-minlength="11" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="policy" class="control-label">Email *</label>
                        <div class="controls">
                            <input value="" type="text" id="email" name="email" class="" data-rule-email="true" data-rule-required="true" />
                        </div>
                    </div>

                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <button type="button" class="btn">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>