<?php
	session_start();
	if ($_SESSION['logged'] != true){
		$msg = base64_encode("Welcome, Please Login!...");
		header("Location: login.php?msg=$msg");
	}
	
	include("../umfcon.inc");
	if (isset($_GET['id'])){
		$id = $_GET['id'];
		$Qchker = "SELECT * FROM tbl_acct_type WHERE acct_type_id= '$id'";
		$Rchker = mysql_query($Qchker);
		if (mysql_num_rows($Rchker) > 0){
			$RowRchker = mysql_fetch_array($Rchker);
		
			$acct_type_id  = $RowRchker['acct_type_id'];
			$acct_type = $RowRchker['acct_type'];
			$charge = $RowRchker['charge'];
			$description = $RowRchker['description'];
			$edit = "1";
		}
	}
?>

	<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h4 id="myModalLabel">Edit Account Type</h4>
	</div>


	<form class="form-horizontal form-bordered" id="frm_edit_user_type" method="post" action="home.php?page=accounts&subpage=process_account&action=edit_acct_type">
		<div class="modal-body nopadding">
			<div class="control-group" style="margin-left: 30px">
				<label for="select" class="control-label">Account Type</label>
				<div class="controls">
					<input type="text" name="acct_type" id="acct_type" class="input text" value="<?php if (isset($acct_type)){
					echo $acct_type;
					}?>"> <input type="hidden" name="acct_type_id" id="acct_type_id" value="<?php if (isset($acct_type_id)){
					echo $acct_type_id;
					}?>" />
				</div>
			</div>
			
			<div class="control-group" style="margin-left: 30px">
				<label for="select" class="control-label">Service Charge</label>
				<div class="controls">
					<input type="text" name="description" id="description" class="input text" value="<?php if (isset($description)){
					echo $description;
					}?>">
				</div>
			</div>
		
			<div class="control-group" style="margin-left: 30px">
				<label for="select" class="control-label">Description</label>
				<div class="controls">
					<input type="text" name="description" id="description" class="input text" value="<?php if (isset($description)){
					echo $description;
					}?>">
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="button" name="btn_edit_user_type" id="btn_edit_user_type" class="btn btn-primary pull-right">Save Changes</button>
		</div>
	</form>



<script>
	
	$("#btn_edit_user_type").click(function() {
		$("#frm_edit_user_type").submit();
		$("#btn_edit_user_type").prop('disabled', true);
    });  

    $('input[type=submit]').click(function(e){
		$('form').submit();
	    $(this).prop('disabled', true);
	});

</script>