<?php
session_start();
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: ../login.php?msg=$msg");
}

include("../methods.php");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $Qchker = "SELECT fs.*, c.first_name, c.last_name FROM 
					tbl_financial_sheet fs, tbl_customer c
					WHERE fs.customer_id = c.customer_id
					AND fs.financial_sheet_id =  '$id'";
    $Rchker = ExecuteSQLQuery($Qchker);

    if (mysqli_num_rows($Rchker) > 0) {
        $RowRchker = mysqli_fetch_array($Rchker);

        $financial_sheet_id = $RowRchker['financial_sheet_id'];
        $first_name = $RowRchker['first_name'];
        $last_name = $RowRchker['last_name'];
        $customer_id = $RowRchker['customer_id'];
        $dl = $RowRchker['dl'];
        $sh = $RowRchker['sh'];
        $sa = $RowRchker['sa'];
        $i = $RowRchker['i'];
        $il = $RowRchker['il'];
        $sec = $RowRchker['sec'];
        $ots = $RowRchker['ots'];
        $loan = $RowRchker['loan'];
        $lrp = $RowRchker['lrp'];
        $sds = $RowRchker['sds'];
        $sdsr = $RowRchker['sdsr'];
        $hajj = $RowRchker['hajj'];
        $month = $RowRchker['month'];
        $year = $RowRchker['year'];
        $edit = "1";
    }
}
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel">Financial Sheet [<?= ucfirst($first_name) . ' ' . ucfirst($last_name); ?>]&nbsp;&nbsp;&nbsp;<div class="label"> <?php echo $months[$month]; ?></div>
        <div class="label"><?php echo $year ?></div> </h4>
</div>


<form class="form-horizontal form-bordered" id="frm_financial_sheet" method="post" action="home.php?page=financial-sheet&subpage=process_sheet&action=edit">
    <div class="modal-body nopadding">

        <table class="table table-striped">

            <div class="control-group">
                <label for="select" class="control-label">Dev. Levy</label>
                <div class="controls">
                    <input type="text" id="dl" name="dl" class="compute" value="<?php
                    if (isset($dl)) {
                        echo $dl;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Shares</label>
                <div class="controls">
                    <input type="text" id="sh" name="sh" class="compute" value="<?php
                    if (isset($sh)) {
                        echo $sh;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Savings</label>
                <div class="controls">
                    <input type="text" id="sa" name="sa" class="compute" value="<?php
                    if (isset($sa)) {
                        echo $sa;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Insurrance</label>
                <div class="controls">
                    <input type="text" id="i" name="i" class="compute" value="<?php
                    if (isset($i)) {
                        echo $i;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Ileya</label>
                <div class="controls">
                    <input type="text" id="il" name="il" class="compute" value="<?php
                    if (isset($il)) {
                        echo $il;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">OTS</label>
                <div class="controls">
                    <input type="text" id="ots" name="ots" class="compute" value="<?php
                    if (isset($ots)) {
                        echo $ots;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">SEC</label>
                <div class="controls">
                    <input type="text" id="sec" name="sec" class="compute" value="<?php
                    if (isset($sec)) {
                        echo $sec;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Loan</label>
                <div class="controls">
                    <input type="text" id="loan" name="loan" class="compute" value="<?php
                    if (isset($loan)) {
                        echo $loan;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Loan Repayment</label>
                <div class="controls">
                    <input type="text" id="lrp" name="lrp" class="compute" value="<?php
                    if (isset($lrp)) {
                        echo $lrp;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group" >
                <label for="select" class="control-label">SDS</label>
                <div class="controls">
                    <input type="text" id="sds" name="sds" class="compute" value="<?php
                    if (isset($sds)) {
                        echo $sds;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">SDS Repayment</label>
                <div class="controls">
                    <input type="text" id="sdsr" name="sdsr" class="compute" value="<?php
                    if (isset($sdsr)) {
                        echo $sdsr;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group" >
                <label for="select" class="control-label">HAJJ</label>
                <div class="controls">
                    <input type="text" id="hajj" name="hajj" class="compute" value="<?php
                    if (isset($hajj)) {
                        echo $hajj;
                    }
                    ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="total" class="control-label">Total</label>
                <div class="controls">
                    <div id="total" name="total" class="" ></div>
                    <input type="hidden" name="total" id="totalFin" />
                </div>
            </div>


        </table>

        <div class="modal-footer">
            <input type="hidden" id="financial_sheet_id" name="financial_sheet_id" value="<?php echo $financial_sheet_id ?>" />
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button type="button" name="btn_update" id="btn_update" class="btn btn-warning pull-right">Update</button>
        </div>
</form>



<script>

    $("#btn_update").click(function () {
        $("#btn_update").prop('disabled', true);
        $("#frm_financial_sheet").submit();
    });

    $('input[type=submit]').click(function (e) {
        $(this).prop('disabled', true);
        $('form').submit();
    });
    
    $('.compute').keyup(function() {
	
			 var total = parseInt($('#hajj').val()) 
			 			+ parseInt($('#sdsr').val()) 
						+ parseInt($('#lrp').val()) 
						+ parseInt($('#loan').val()) 
						+ parseInt($('#ots').val()) 
						+ parseInt($('#sec').val()) 
						+ parseInt($('#il').val()) 
						+ parseInt($('#i').val()) 
						+ parseInt($('#sa').val()) 
						+ parseInt($('#sh').val()) 
						+ parseInt($('#dl').val()) 
						+ parseInt($('#sds').val()) ;
						
            $('#total').html('<b>'+total+'</b>');
            $('#totalFin').val(total);
			return false;
		
	});	

</script>