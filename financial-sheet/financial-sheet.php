<?php if ($_SESSION['logged'] != true){
		$msg = base64_encode("Welcome, Please Login!...");
		header("Location: ../login.php?msg=$msg");
	}
	
	?>

<?php include './_notification.php'; ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Financial Sheet</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        
    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Financial Sheet</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <h3>
			<a href="home.php?page=financial-sheet&subpage=add" class="btn btn-warning">Add Record</a>
        </h3>
       
    </div>
    <div class="box-content nopadding">
        <div class="tab-content">
           
            
            <div class="tab-pane active" id="enrolled">
                
                
                <table class="table table-user table-hover table-nomargin dataTable">
                                <thead>
                                        <tr>
                                                <th>SN</th>
                                                <th>Name</th>
                                                <th class='hidden-1024'>DL</th>
                                                <th class='hidden-480'>Shares</th>
                                                <th class='hidden-480'>Savings</th>
                                                <th class='hidden-480'>Insurrance</th>
                                                <th class='hidden-480'>Ileya</th>
                                                <th>Total</th>
                                                <th class='hidden-480'>Month</th>
                                                <th class='hidden-480'>Year</th>
                                                <th class='hidden-480' nowrap>Action</th>
                                        </tr>
                                </thead>
                                <tbody>
                                <?php 
									$Rqry = ExecuteSQLQuery("SELECT fs.*, fs.dl+fs.sh+fs.sa+fs.i+fs.il+fs.sec+fs.ots+fs.loan+fs.lrp+fs.sds+fs.sdsr+fs.hajj total , c.first_name, c.last_name
												FROM tbl_financial_sheet fs
												INNER JOIN tbl_customer c ON fs.customer_id = c.customer_id");
									  $counter = 1;
									  while ($rowRqry = mysqli_fetch_array($Rqry)){  ?>
										<tr>
											<td> <?php echo $counter ?>  </td>
                                            <td> <?= ucfirst($rowRqry["first_name"]) .' ' . ucfirst($rowRqry["last_name"]); ?></td>
											<td> <?php echo number_format($rowRqry["dl"],2); ?> </td>
											<td><?php echo number_format($rowRqry["sh"],2); ?></td>
											<td><?php echo number_format($rowRqry["sa"],2); ?></td>
											<td><?php echo number_format($rowRqry["i"],2); ?></td>
											<td><?php echo number_format($rowRqry["il"],2); ?></td>
											<td><?php echo number_format($rowRqry["total"],2); ?></td>
											<td><?php echo $months[$rowRqry["month"]]; ?></td>
											<td><?php echo $rowRqry["year"]; ?></td>
											<td>
													<div class="btn-group">
															<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
															<ul class="dropdown-menu">
																	<li><a href="financial-sheet/view_record.php?id=<?php echo $rowRqry["financial_sheet_id"]?>" class="ajaxlink_view_user">View </a></li>
																	<li><a href="financial-sheet/edit_financial_sheet.php?id=<?php echo $rowRqry["financial_sheet_id"]?>" class="ajaxlink_user">Edit </a></li>
																	<li><a href="home.php?page=financial-sheet&subpage=process_sheet&action=delete_sheet&id=<?php echo $rowRqry["financial_sheet_id"]?>" class="modal_confirm">Delete </a></li>
																	
															</ul>
													</div>
											</td>
									</tr>
                                <?php $counter++; } ?>

                            </tbody>
                        </table>
                
                
            </div><!-- End Div Enrolled -->
			 

        </div>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-body">
    <p>Are you sure you want to delete this record?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="#" class="btn btn-danger closeme">Yes, delete</a>
  </div>
</div>
<!-- End Remove Shift Modal -->

<!-- Edit user dialog -->
<div id="edit_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<!-- View user dialog -->
<div id="view_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script type="text/javascript">
    
    $('.modal_confirm').click(function(eve){
        
        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();
        
        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
        
    });
    
    
    
	 $('.ajaxlink_view_user').click(function(eve){
        
      	eve.preventDefault();
        $('#view_user_modal').modal('show');
        $('#view_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#view_user_modal').html('');
          $('#view_user_modal').html(html).show();

          
        });
        
    });
    
    
    $('.ajaxlink_user').click(function(eve){
        
      	eve.preventDefault();
        $('#edit_user_modal').modal('show');
        $('#edit_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#edit_user_modal').html('');
          $('#edit_user_modal').html(html).show();

          
        });
        
    });

    
</script>