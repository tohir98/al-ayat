<?php
	
	if($_GET["action"] == "add"){
	
		$dl = $_POST['dl'];
		$sh = $_POST['sh'];
		$sa = $_POST['sa'];
		$i = $_POST['i'];
		$il =  $_POST['il'];
		$sec = $_POST['sec'];
		$ots = $_POST['ots'];
		$loan = $_POST['loan'];
		$lrp =  $_POST['lrp'];
		$sds =  $_POST['sds'];
		$file = $_POST['file'];
		$sdsr = $_POST['sdsr'];
		$hajj = $_POST['hajj'];
		$customer_id = $_POST['customer_id'];
		$month = date('m'); //$_POST['month'];
		$year = date('Y');
		$date_created = date('Y-m-d h:i:s');
		
		$created_by = $_SESSION['role_id'];
		$date_created = date('Y-m-d h:i:s');
		
		if (!check_financial($customer_id, $month, $year)) {
			$q = mysql_query("INSERT INTO tbl_financial_sheet (dl, sh, sa, i, il, sec, ots, loan, lrp, sds, sdsr, hajj, customer_id, month, year, created_by, date_created) VALUES('$dl', '$sh', '$sa', '$i', '$il', '$sec', '$ots', '$loan', '$lrp', '$sds', '$sdsr', '$hajj', '$customer_id', '$month', '$year', '$created_by', '$date_created')");
			if ($q)
				$_SESSION['success'] = "Operation was successful";
				//send_email();
			else
				$_SESSION['error'] = "Error saving user record";
		}else{
			$_SESSION['error'] = "Record already exist";
		}//End if check
		
		header('location:home.php?page=financial-sheet');
	}//End if action
	
	
	//adduser_type
	else if($_GET["action"] == "adduser_type"){
	
		$user_type = $_POST['user_type'];
		$user_desc = $_POST['user_desc'];
		
		if (!check('tbl_user_type','user_type',$user_type)) {
			$q = mysql_query("INSERT INTO tbl_user_type (user_type, user_desc) VALUES('$user_type', '$user_desc')");
			if ($q)
				$_SESSION['success'] = "User type created successfully";
			else
				$_SESSION['error'] = "Error creating user type";
		}else{
			$_SESSION['error'] = "User type already exist";
		}//End if check
		
		header('location:home.php?page=users&subpage=user_type');
	}//End if action = adduser_type
	
		//add_location
	else if($_GET["action"] == "add_location"){
	
		$location_tag = $_POST['location_tag'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state_id = $_POST['state_id'];
		$parent_id = $_POST['parent_id'];
		$head_id = $_POST['head_id'];
		$date_created = date('Y-m-d h:i:s');
		$creator_id = $_SESSION['role_id'];
		
		if (!check('tbl_locations','location_tag',$location_tag)) {
			$q = mysql_query("INSERT INTO tbl_locations (location_tag, address, city, state_id, parent_id, head_id, date_created, creator_id) VALUES('$location_tag', '$address', '$city', '$state_id', '$parent_id', '$head_id', '$date_created', '$creator_id')");
			if ($q)
				$_SESSION['success'] = "Location added successfully";
			else
				$_SESSION['error'] = "Error adding location";
		}else{
			$_SESSION['error'] = "Location already exist";
		}//End if check
		
		header('location:home.php?page=users&subpage=locations');
	}//End if action = add_location
	
	
	//edit_user_type
	else if($_GET["action"] == "edit_user_type"){
	
		$user_type = $_POST['user_type'];
		$user_desc = $_POST['user_desc'];
		$user_type_id = $_POST['user_type_id'];
		
		$q = mysql_query("UPDATE tbl_user_type SET  user_type = '$user_type', user_desc = '$user_desc' WHERE user_type_id = '$user_type_id' ");
			if ($q)
				$_SESSION['success'] = "User type updated successfully";
			else
				$_SESSION['error'] = "Error updating user type";
		
		header('location:home.php?page=users&subpage=user_type');
	}//End if action = edit_user_type
	
	
	//edit_user
	else if($_GET["action"] == "edit_user"){
	
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$user_type_id = $_POST['user_type_id'];
		$role_id = $_POST['role_id'];
		$location_id = $_POST['location_id'];
		
		$q = mysql_query("UPDATE tbl_role SET  Firstname = '$first_name', Lastname = '$last_name', email = '$email', user_type_id = '$user_type_id', location_id = '$location_id'  WHERE role_id = '$role_id' ");
			if ($q)
				$_SESSION['success'] = "User updated successfully";
			else
				$_SESSION['error'] = "Error updating user info";
		
		header('location:home.php?page=users');
	}//End if action = edit_user
	
	//edit_location
	else if($_GET["action"] == "edit_location"){
	
		$location_tag = $_POST['location_tag'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state_id = $_POST['state_id'];
		$parent_id = $_POST['parent_id'];
		$head_id = $_POST['head_id'];
		$location_id = $_POST['location_id'];
		
		$q = mysql_query("UPDATE tbl_locations SET  location_tag = '$location_tag'
							, address = '$address'
							, city = '$city'
							, state_id = '$state_id' 
							, parent_id = '$parent_id' 
							, head_id = '$head_id'
							WHERE location_id = '$location_id' ");
			if ($q)
				$_SESSION['success'] = "Location updated successfully";
			else
				$_SESSION['error'] = "Error updating location";
		
		header('location:home.php?page=users&subpage=locations');
	}//End if action = edit_location
	
	
	else if ($_GET["action"] == "delete_user"){
	
		$id = $_GET['id'];
	
		$where = 'role_id ='. $id;
		$q = delete('tbl_role',$where);
		
		if($q){
			$_SESSION['success'] = "User deleted successfully";
		}else{
			$_SESSION['error'] = "Error while deleting user account"; //mysql_error();
		}
		
		header('location:home.php?page=users');
		
	}//End if action is delete
	
	
	else if ($_GET["action"] == "delete_user_type"){
	
		$id = $_GET['id'];
	
		$where = 'user_type_id ='. $id;
		$q = delete('tbl_user_type',$where);
		
		if($q){
			$_SESSION['success'] = "User type deleted successfully";
		}else{
			$_SESSION['error'] = "Error while deleting user type"; 
		}
		
		header('location:home.php?page=users&subpage=user_type');
		
	}//End if action is delete_user_type
	
		else if ($_GET["action"] == "delete_location"){
	
		$id = $_GET['id'];
	
		$where = 'location_id ='. $id;
		$q = delete('tbl_locations',$where);
		
		if($q){
			$_SESSION['success'] = "Location deleted successfully";
		}else{
			$_SESSION['error'] = "Error while deleting location"; 
		}
		
		header('location:home.php?page=users&subpage=locations');
		
	}//End if action is delete_user_type
	
	
	
	
?>