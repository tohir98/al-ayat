<?php
	session_start();
	
	$msg = '';
	$msg = base64_decode($_REQUEST["msg"]);
	
	if (!get_cfg_var('safe_mode')) {
      set_time_limit(0);
    }
	
	include("umfcon.inc");
	$_SESSION['loggedin'] = false;
	
	function isLogged(){ 
		if($_SESSION['logged']){ # When logged in this variable is set to TRUE 
			return true; 
		}else{ 
			return false; 
		} 
	} 
	
	function logOut(){ 
		$_SESSION = array(); 
		if (isset($_COOKIE[session_name()])) { 
			setcookie(session_name(), '', time()-42000, '/'); 
		} 
		session_destroy(); 
    } 
      
				
		
	
	
	if(isset($_POST['btn']))
	{		
		
		$uname = strip_tags(stripslashes($_POST['uemail']));
		$pwd = strip_tags(stripslashes($_POST['upw']));
		
		/*echo $uname."<br>";
		echo $pwd."<br>";
		die();*/
			  
		$qry = "SELECT * FROM tbl_role WHERE email='$uname' and Password='$pwd' and islocked != 'yes'";
		
		$rst = mysqli_query($dbhandle, $qry) or die(mysql_error());
		
		if($row = mysqli_fetch_assoc($rst))
		{
			$user_type_id = trim($row["user_type_id"]);

		// To retrieve the user type	---------------------------
		$role = 0;
		$Quser = "select * from tbl_user_type where user_type_id = '$user_type_id' ";
		$Ruser = mysqli_query($dbhandle, $Quser);
		while($row_rsResult = mysqli_fetch_array($Ruser)) 
		{ 
			$role=$row_rsResult['user_type'];
		}
			
			$_SESSION['email'] = $uname;
			$_SESSION['Fstname'] = $row["Firstname"];
			$_SESSION['role_id'] = $row["role_id"];
			$_SESSION['location_id'] = $row["location_id"];
			$_SESSION['pwd']= $pwd;
			$_SESSION['fname'] = trim($row["Firstname"])." ".trim($row["Lastname"]);
			$_SESSION['uid'] = $row["email"];
			$_SESSION['logged'] = true;
			
						
			switch($role)
			{
				case "admin":
				//go to Admin's page
				
				header("Location: home.php");
				$qry1 = "update tbl_role set islocked = 'yes' where (Username='$uname' and Password='$pwd') ";
				mysql_query($qry1) or die(mysql_error());
				break;
														
				case "Supervisor":
				//go to user's  page
				header("Location: home.php");
				$qry3 = "update tbl_role set islocked = 'yes' where (Username='$uname' and Password='$pwd') ";
				mysql_query($qry3) or die(mysql_error());
				break;
				
				default:
				$msg = "Sorry, you don't have proviledge to access this account";
			}
			
		}elseif ($islocked = 'yes')
		{
			$msg = "Access Denied, Contact the Administrator";
		}
		else
		{
			$msg = "Invalid Login details";
		}
	}
?>


<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>Al-Ayat Co-operative Society - Login</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
	<!-- icheck -->
	<link rel="stylesheet" href="css/plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="css/themes.css">


	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	
	<!-- Nice Scroll -->
	<script src="js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- Validation -->
	<script src="js/plugins/validation/jquery.validate.min.js"></script>
	<script src="js/plugins/validation/additional-methods.min.js"></script>
	<!-- icheck -->
	<script src="js/plugins/icheck/jquery.icheck.min.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/eakroko.js"></script>

	<!--[if lte IE 9]>
		<script src="js/plugins/placeholder/jquery.placeholder.min.js"></script>
		<script>
			$(document).ready(function() {
				$('input, textarea').placeholder();
			});
		</script>
	<![endif]-->
	

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/Checked.ico">
	<!-- Apple devices Homescreen icon -->
	<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />

</head>

<body class='login'>
	<div class="wrapper">
		<h1><a href="index.html"><img src="img/logo-big.png" alt="" class='retina-ready' width="59" height="49">Al-Ayat Coop</a></h1>
		<div class="login-body">
			<h2>SIGN IN</h2>
			<div align="center"><?php echo $msg; ?> </div>
			<form method='post' class='form-validate' id="frmlogin" action="">
				<div class="control-group">
					<div class="email controls">
						<input type="text" name='uemail' placeholder="Email address" class='input-block-level' data-rule-required="true" data-rule-email="true">
					</div>
				</div>
				<div class="control-group">
					<div class="pw controls">
						<input type="password" name="upw" placeholder="Password" class='input-block-level' data-rule-required="true">
					</div>
				</div>
				<div class="submit">
					<div class="remember">
						<input type="checkbox" name="remember" class='icheck-me' data-skin="square" data-color="blue" id="remember"> <label for="remember">Remember me</label>
					</div>
					<input type="submit" name="btn" id="btn" value="Log me in" class='btn btn-primary'>
				</div>
			</form>
			<div class="forget">
				<a href="#"><span>Forgot password?</span></a>
			</div>
		</div>
	</div>
	
</body>

</html>
