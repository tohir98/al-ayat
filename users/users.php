<?php
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: ../login.php?msg=$msg");
}
?>

<?php include './_notification.php'; ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Users</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php?page=dashboard">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="">Users</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Users</a>
        </li>
    </ul>
</div>


<div class="box box-bordered ">
    <div class="box-title">
        <h3>
            <button href="#create_new_user_modal" data-toggle="modal" class="btn btn-warning">Create New User</button>
        </h3>

    </div>
    <div class="box-content-padless">
        <table class="table table-user table-hover table-nomargin dataTable table-condensed">
            <thead>
                <tr>
                    <th>SN</th>
                    <th>Name</th>
                    <th class='hidden-1024'>Email</th>
                    <th class='hidden-480'>User Type</th>
                    <th class='hidden-480' nowrap>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $Rqry = mysqli_query($dbhandle, "SELECT r.* , ut.user_type, lo.location_tag
												FROM tbl_role r
												LEFT JOIN tbl_user_type ut ON r.user_type_id = ut.user_type_id
												LEFT JOIN tbl_locations lo ON r.location_id = lo.location_id");
                $counter = 1;
                while ($rowRqry = mysqli_fetch_array($Rqry)) {
                    ?>
                    <tr>
                        <td> <?php echo $counter ?>  </td>
                        <td> <?php echo $rowRqry["Firstname"]; ?> <?php echo $rowRqry["Lastname"]; ?> 
                            <br><span class="muted"><?php echo $rowRqry["location_tag"]; ?>
                        </td>
                        <td> <?php echo $rowRqry["email"]; ?> </td>
                        <td>
                            <span class="muted"><?php echo $rowRqry["user_type"]; ?></span>
                        </td>
                        <td>
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="users/view_user.php?id=<?php echo $rowRqry["role_id"] ?>" class="ajaxlink_view_user">View Records </a></li>
                                    <li><a href="users/edit_user.php?id=<?php echo $rowRqry["role_id"] ?>" class="ajaxlink_user">Edit User </a></li>
                                    <li><a href="home.php?page=users&subpage=process_user&action=delete_user&id=<?php echo $rowRqry["role_id"] ?>" class="modal_confirm">Delete User </a></li>

                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $counter++;
                }
                ?>

            </tbody>
        </table>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-body">
        <p>Are you sure you want to delete user?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, delete user</a>
    </div>
</div>
<!-- End Remove Shift Modal -->


<!-- Add User Modal -->
<div class="modal hide fade" id="create_new_user_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        <h3 class="modal-title" id="myModalLabel">Create User</h3>
    </div>

    <form name="add_user" id="add_user" method="post" action="home.php?page=users&subpage=process_user&action=add" class="form-horizontal form-bordered">
        <div class="modal-body nopadding">
            <div class="control-group" style="margin-left: 30px">
                <label for="select" class="control-label">First Name</label>
                <div class="controls">
                    <input type="text" name="first_name" id="first_name" class="input text">
                </div>
            </div>

            <div class="control-group" style="margin-left: 30px">
                <label for="select" class="control-label">LastName</label>
                <div class="controls">
                    <input type="text" name="last_name" id="last_name" class="input text">
                </div>
            </div>

            <div class="control-group" style="margin-left: 30px">
                <label for="select" class="control-label">Email</label>
                <div class="controls">
                    <input type="text" name="Email" id="Email" class="input text">
                </div>
            </div>

            <div class="control-group" style="margin-left: 30px">
                <label for="select" class="control-label">User Type</label>
                <div class="controls">
                    <?php
                    $Qemp2 = "SELECT * FROM tbl_user_type";
                    $Remp2 = mysqli_query($dbhandle, $Qemp2);
                    ?>
                    <select name="user_type_id" id="user_type_id" class="select2-me input-xlarge">
                        <option value="0">Select user type</option>
                        <?php while ($Rowsemp2 = mysqli_fetch_array($Remp2)) { ?>
                            <option value="<?php echo $Rowsemp2['user_type_id']; ?>"><?php echo $Rowsemp2['user_type']; ?></option>
<?php } ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="modal-footer">
            <input type="submit" class="btn btn-primary" value="Create User">
            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i> Cancel </button>
        </div>
    </form>
</div>

<!-- Edit user dialog -->
<div id="edit_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<!-- View user dialog -->
<div id="view_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script type="text/javascript">

    $('.modal_confirm').click(function (eve) {

        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });



    $('.modal_confirm_shift').click(function (eve) {

        eve.preventDefault();
        $('#modal-3').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });


    $('.ajaxlink_view_user').click(function (eve) {

        eve.preventDefault();
        $('#view_user_modal').modal('show');
        $('#view_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#view_user_modal').html('');
            $('#view_user_modal').html(html).show();


        });

    });


    $('.ajaxlink_user').click(function (eve) {

        eve.preventDefault();
        $('#edit_user_modal').modal('show');
        $('#edit_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#edit_user_modal').html('');
            $('#edit_user_modal').html(html).show();


        });

    });


</script>