<?php
	session_start();
	if ($_SESSION['logged'] != true){
		$msg = base64_encode("Welcome, Please Login!...");
		header("Location: ../login.php?msg=$msg");
	}
	
	include("../umfcon.inc");
	if (isset($_GET['id'])){
	$id = $_GET['id'];
	$Qchker = "SELECT * FROM tbl_user_type WHERE user_type_id= '$id'";
	$Rchker = mysqli_query($dbhandle, $Qchker);
	if (mysqli_num_rows($Rchker) > 0){
	$RowRchker = mysqli_fetch_array($Rchker);
	
	$user_type_id   = $RowRchker['user_type_id'];
	$user_type = $RowRchker['user_type'];
	$user_desc = $RowRchker['user_desc'];
	$edit = "1";
	}}
?>

	<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h4 id="myModalLabel">Edit User Type</h4>
	</div>


	<form class="form-horizontal form-bordered" id="frm_edit_user_type" method="post" action="home.php?page=users&subpage=process_user&action=edit_user_type">
		<div class="modal-body nopadding">
			<div class="control-group" style="margin-left: 30px">
				<label for="select" class="control-label">User Type</label>
				<div class="controls">
					<input type="text" name="user_type" id="user_type" class="input text" value="<?php if (isset($user_type)){
					echo $user_type;
					}?>"> <input type="hidden" name="user_type_id" id="user_type_id" value="<?php if (isset($user_type_id)){
					echo $user_type_id;
					}?>" />
				</div>
			</div>
		
			<div class="control-group" style="margin-left: 30px">
				<label for="select" class="control-label">Description</label>
				<div class="controls">
					<input type="text" name="user_desc" id="user_desc" class="input text" value="<?php if (isset($user_desc)){
					echo $user_desc;
					}?>">
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="button" name="btn_edit_user_type" id="btn_edit_user_type" class="btn btn-primary pull-right">Save Changes</button>
		</div>
	</form>



<script>
	
	$("#btn_edit_user_type").click(function() {
		$("#frm_edit_user_type").submit();
		$("#btn_edit_user_type").prop('disabled', true);
    });  

    $('input[type=submit]').click(function(e){
		$('form').submit();
	    $(this).prop('disabled', true);
	});

</script>