<!-- Display error/success message -->

<?php if (isset($_SESSION['error']) && $_SESSION['error'] != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $_SESSION['error']; ?>
    </div>
    <?php $_SESSION['error'] = '';
}

if ($_SESSION['success'] != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
    <?php echo $_SESSION['success']; ?>
    </div>
    <?php $_SESSION['success'] = '';
} ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>User Type</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php?page=users">Users</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>User Types</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <h3>
            <button href="#add_user_type_modal" data-toggle="modal" class="btn btn-warning">Add User Type</button>
        </h3>

    </div>
    <div class="box-content nopadding">
        <div class="tab-content">


            <div class="tab-pane active" id="enrolled">


                <table class="table table-user table-hover table-nomargin dataTable table-condensed">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>User Type</th>
                            <th class='hidden-1024'>Description</th>
                            <th class='hidden-480' nowrap>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $Rqry = ExecuteSQLQuery("SELECT * FROM tbl_user_type");
                        $counter = 1;
                        while ($rowRqry = mysqli_fetch_array($Rqry)) {
                            ?>
                            <tr>
                                <td> <?php echo $counter ?>  </td>
                                <td> <?php echo ucfirst($rowRqry["user_type"]); ?></td>
                                <td class='hidden-1024'> <?php echo $rowRqry["user_desc"]; ?> </td>
                                <td class='hidden-480'>
                                    <div class="btn-group">
                                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="users/edit_user_type.php?id=<?php echo $rowRqry["user_type_id"] ?>" class="ajaxlink_user_type">Edit User Type </a></li>
                                            <li><a href="home.php?page=users&subpage=process_user&action=delete_user_type&id=<?php echo $rowRqry["user_type_id"] ?>" class="modal_confirm">Delete User Type </a></li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
    <?php $counter++;
} ?>

                    </tbody>
                </table>


            </div><!-- End Div Enrolled -->


        </div>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-body">
        <p>Are you sure you want to delete user?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, delete user type</a>
    </div>
</div>
<!-- End Remove Shift Modal -->


<!-- Add User Modal -->
<div class="modal hide fade" id="add_user_type_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">

    <div class="row_fluid"> 
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            <h4 class="modal-title" id="myModalLabel">Add User Type</h4>
        </div>

        <form name="add_user_type" id="add_user_type" method="post" action="home.php?page=users&subpage=process_user&action=adduser_type" class="form-horizontal form-bordered">
            <div class="modal-body nopadding">
                <div class="control-group" style="margin-left: 30px">
                    <label for="select" class="control-label">User Type</label>
                    <div class="controls">
                        <input type="text" name="user_type" id="user_type" class="input text">
                    </div>
                </div>

                <div class="control-group" style="margin-left: 30px">
                    <label for="select" class="control-label">Description</label>
                    <div class="controls">
                        <input type="text" name="user_desc" id="user_desc" class="input text">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Add User Type</button>
                <button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>


        </form>
    </div>
</div>


<div id="contribution" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script type="text/javascript">

    $('.modal_confirm').click(function (eve) {

        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });



    $('.modal_confirm_shift').click(function (eve) {

        eve.preventDefault();
        $('#modal-3').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });



    $('.ajaxlink_user_type').click(function (eve) {

        eve.preventDefault();
        $('#contribution').modal('show');
        $('#contribution').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#contribution').html('');
            $('#contribution').html(html).show();


        });

    });
</script>