<!-- Display error/success message -->

<?php include './_notification.php'; ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Locations</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        
    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php?page=users">Users</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Locations</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <div class="pull-right" style="margin-right: 5px">
			<button href="#create_location_modal" data-toggle="modal" class="btn btn-warning">Add New Location</button>
		</div>
    </div>
    <div class="box-content nopadding">
        <div class="tab-content">
           
            
            <div class="tab-pane active" id="enrolled">
                
                
                <table class="table table-user table-hover table-nomargin dataTable table-condensed">
                                <thead>
                                        <tr>
                                                <th>SN</th>
                                                <th>Location</th>
                                                <th class='hidden-1024'>Address</th>
                                                <th class='hidden-1024'>Manager</th>
                                                <th class='hidden-480' nowrap>Action</th>
                                        </tr>
                                </thead>
                                <tbody>
                                <?php 
									$Rqry = mysqli_query($dbhandle, "SELECT l.* , s.StateName, r.Firstname, r.Lastname
														from tbl_locations l
														LEFT JOIN tbl_states s ON l.state_id = s.StateID
														LEFT JOIN tbl_role r ON l.head_id = r.role_id");
									  $counter = 1;
									  while ($rowRqry = mysqli_fetch_array($Rqry)){  ?>
										<tr>
											<td> <?php echo $counter ?>  </td>
											<td> <?php echo $rowRqry["location_tag"]; ?> </td>
											<td> <?php echo $rowRqry["address"]; ?> <br> 
												<span class="muted"><?php echo $rowRqry["city"]; ?></span><br>
												<span class="muted"><?php echo $rowRqry["StateName"]; ?></span>
											<td> <?php echo $rowRqry["Firstname"] .' '. $rowRqry["Lastname"]; ?> </td>
											</td>
											<td>
													<div class="btn-group">
															<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
															<ul class="dropdown-menu">
																	<li><a href="users/edit_location.php?id=<?php echo $rowRqry["location_id"]?>" class="ajaxlink_user">Edit </a></li>
																	<li><a href="home.php?page=users&subpage=process_user&action=delete_location&id=<?php echo $rowRqry["location_id"]?>" class="modal_confirm">Delete </a></li>
																	
															</ul>
													</div>
											</td>
									</tr>
                                <?php $counter++; } ?>

                            </tbody>
                        </table>
                
                
            </div><!-- End Div Enrolled -->
			 

        </div>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-body">
    <p>Are you sure you want to delete location?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="#" class="btn btn-danger closeme">Yes, delete location</a>
  </div>
</div>
<!-- End Remove Shift Modal -->


<!-- Add Locaion Modal -->
<div class="modal hide fade" id="create_location_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
     
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 class="modal-title" id="myModalLabel">Add Location</h3>
    </div>
    
                <form name="add_location" id="add_location" method="post" action="home.php?page=users&subpage=process_user&action=add_location" class="form-horizontal form-bordered">
					<div class="modal-body nopadding"  style="height:300px">
                        <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Location Tag</label>
                            <div class="controls">
                                <input type="text" name="location_tag" id="location_tag" class="input text">
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Address</label>
                            <div class="controls">
                                <input type="text" name="address" id="address" class="input text">
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">City</label>
                            <div class="controls">
                                <input type="text" name="city" id="city" class="input text">
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">State</label>
                            <div class="controls">
								<?php
									$Qemp2 = "SELECT * FROM tbl_states";
									$Remp2 = mysqli_query($dbhandle, $Qemp2);
								?>
								<select name="state_id" id="state_id" class="select2-me input-large">
									<option value="0">Select state</option>
									<?php while($Rowsemp2=mysqli_fetch_array($Remp2)){ ?>
									<option value="<?php echo $Rowsemp2['StateID']; ?>"><?php echo $Rowsemp2['StateName'];?></option>
									<?php } ?>
								</select>
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Parent Location</label>
                            <div class="controls">
								<?php $Rloc = mysqli_query($dbhandle, "SELECT * FROM tbl_locations");?>
								<select name="parent_id" id="parent_id" class="select2-me input-large">
									<option value="0">Select parent</option>
									<?php while($Rowsloc=mysqli_fetch_array($Rloc)){ ?>
									<option value="<?php echo $Rowsloc['location_id']; ?>"><?php echo $Rowsloc['location_tag'];?></option>
									<?php } ?>
								</select>
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Location Head</label>
                            <div class="controls">
								<?php $Rusr = mysqli_query($dbhandle, "SELECT * FROM tbl_role");?>
								<select name="parent_id" id="parent_id" class="select2-me input-large">
									<option value="0">Select parent</option>
									<?php while($Rowsusr=mysqli_fetch_array($Rusr)){ ?>
									<option value="<?php echo $Rowsusr['role_id']; ?>"><?php echo trim($Rowsusr["Firstname"])." ".trim($Rowsusr["Lastname"]);?></option>
									<?php } ?>
								</select>
                            </div>
                        </div>
					</div>

					<div class="modal-footer">
						<input type="submit" class="btn btn-primary" value="Add Location">
					</div>
				</form>
</div>

<!-- Edit user dialog -->
<div id="edit_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<!-- View user dialog -->
<div id="view_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script type="text/javascript">
    
    $('.modal_confirm').click(function(eve){
        
        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();
        
        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
        
    });
    
    
    
    $('.modal_confirm_shift').click(function(eve){
        
        eve.preventDefault();
        $('#modal-3').modal('show').fadeIn();
        
        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
        
    });
    
    
	 $('.ajaxlink_view_user').click(function(eve){
        
      	eve.preventDefault();
        $('#view_user_modal').modal('show');
        $('#view_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#view_user_modal').html('');
          $('#view_user_modal').html(html).show();

          
        });
        
    });
    
    
    $('.ajaxlink_user').click(function(eve){
        
      	eve.preventDefault();
        $('#edit_user_modal').modal('show');
        $('#edit_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#edit_user_modal').html('');
          $('#edit_user_modal').html(html).show();

          
        });
        
    });

    
</script>