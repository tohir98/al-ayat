<?php
	session_start();
	if ($_SESSION['logged'] != true){
		$msg = base64_encode("Welcome, Please Login!...");
		header("Location: ../login.php?msg=$msg");
	}
	
	include("../umfcon.inc");
	if (isset($_GET['id'])){
		$id = $_GET['id'];
		$Qchker = "SELECT * FROM tbl_locations WHERE location_id= '$id'";
		$Rchker = mysqli_query($dbhandle, $Qchker);
		if (mysqli_num_rows($Rchker) > 0){
			$RowRchker = mysqli_fetch_array($Rchker);
			
			$location_id   = $RowRchker['location_id'];
			$address = $RowRchker['address'];
			$city = $RowRchker['city'];
			$state_id = $RowRchker['state_id'];
			$location_tag = $RowRchker['location_tag'];
			$head_id = $RowRchker['head_id'];
			$parent_id = $RowRchker['parent_id'];
			$edit = "1";
		}
	}
?>

	<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h4 id="myModalLabel">Edit Location</h4>
	</div>


	<form class="form-horizontal form-bordered" id="frm_edit_location" method="post" action="home.php?page=users&subpage=process_user&action=edit_location">
		<div class="modal-body nopadding">
		
			<div class="control-group" style="margin-left: 30px">
				<label for="location_tag" class="control-label">Location Tag</label>
				<div class="controls">
					<input type="text" name="location_tag" id="location_tag" class="input text" value="<?php if (isset($location_tag)){
					echo $location_tag;
					}?>">
					<input type="hidden" name="location_id" value="<?php echo $location_id ?>">
				</div>
			</div>
			
			<div class="control-group" style="margin-left: 30px">
				<label for="address" class="control-label">Address</label>
				<div class="controls">
					<input type="text" name="address" id="address" class="input text" value="<?php if (isset($address)){
					echo $address;
					}?>">
				</div>
			</div>
			
			<div class="control-group" style="margin-left: 30px">
				<label for="city" class="control-label">City</label>
				<div class="controls">
					<input type="text" name="city" id="city" class="input text" value="<?php if (isset($city)){
					echo $city;
					}?>">
				</div>
			</div>
			
			 <div class="control-group" style="margin-left: 30px">
				<label for="state" class="control-label">State</label>
				<div class="controls">
					<?php 
						if (isset($state_id)){
							$Rloc2 = mysqli_query($dbhandle, "SELECT * FROM tbl_states WHERE StateID != '$state_id'");
							$Rloc1 = mysqli_query($dbhandle, "SELECT * FROM tbl_states WHERE StateID = '$state_id'");
							$rowloc1 = mysqli_fetch_array($Rloc1);
						}else{
							$Rloc2 = mysqli_query($dbhandle, "SELECT * FROM tbl_states");
						}
					?>

					<select name="state_id" id="state_id" class="select2-me input-large">
						<option value="0">Select state</option>
						<?php if (isset($state_id)){?>
							<option  selected="selected" value="<?php echo $rowloc1['StateID']; ?>"><?php echo $rowloc1['StateName'];?></option>
						<?php }?>
						<?php while($Rowsloc2=mysql_fetch_array($Rloc2)){ ?>
							<option value="<?php echo $Rowsloc2['StateID']; ?>"><?php echo $Rowsloc2['StateName'];?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		
			<div class="control-group" style="margin-left: 30px">
				<label for="state" class="control-label">Parent Location</label>
				<div class="controls">
					<?php 
						if (isset($parent_id)){
							$Rloc2 = mysqli_query($dbhandle, "SELECT * FROM tbl_locations WHERE location_id != '$location_id'");
							$Rloc1 = mysqli_query($dbhandle, "SELECT * FROM tbl_locations WHERE location_id = '$location_id'");
							$rowloc1 = mysqli_fetch_array($Rloc1);
						}else{
							$Rloc2 = mysql_query($dbhandle, "SELECT * FROM tbl_locations");
						}
					?>

					<select name="parent_id" id="parent_id" class="select2-me input-large">
						<option value="0">Select state</option>
						<?php if (isset($parent_id)){?>
							<option  selected="selected" value="<?php echo $rowloc1['location_id']; ?>"><?php echo $rowloc1['location_tag'];?></option>
						<?php }?>
						<?php while($Rowsloc2=mysqli_fetch_array($Rloc2)){ ?>
							<option value="<?php echo $Rowsloc2['location_id']; ?>"><?php echo $Rowsloc2['location_tag'];?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="control-group" style="margin-left: 30px">
				<label for="state" class="control-label">Location Head</label>
				<div class="controls">
					<?php 
						if (isset($head_id)){
							$Rhea2 = mysqli_query($dbhandle, "SELECT * FROM tbl_role WHERE role_id != '$head_id'");
							$Rhea1 = mysqli_query($dbhandle, "SELECT * FROM tbl_role WHERE role_id = '$head_id'");
							$rowhea1 = mysqli_fetch_array($Rhea1);
						}else{
							$Rloc2 = mysqli_query($dbhandle, "SELECT * FROM tbl_role");
						}
					?>

					<select name="head_id" id="head_id" class="select2-me input-large">
						<option value="0">Select Location Head</option>
						<?php if (isset($head_id) && $head_id > 0){?>
							<option  selected="selected" value="<?php echo $rowhea1['role_id']; ?>"><?php echo $rowhea1['Firstname'];?><?php echo $rowhea1['Lastname'];?></option>
						<?php }?>
						<?php 
                        while($Rowshea2=mysqli_fetch_array($Rhea2)){ ?>
							<option value="<?php echo $Rowshea2['role_id']; ?>"><?= $Rowshea2['Firstname'] . ' '  .$Rowshea2['Lastname'];?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
		</div>

		<div class="modal-footer">
			<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="button" name="btn_edit_location" id="btn_edit_location" class="btn btn-primary pull-right">Save Changes</button>
		</div>
	</form>



<script>
	
	$("#btn_edit_location").click(function() {
		$("#frm_edit_location").submit();
		$("#btn_edit_location").prop('disabled', true);
    });  

    $('input[type=submit]').click(function(e){
		$('form').submit();
	    $(this).prop('disabled', true);
	});

</script>