CREATE TABLE  IF NOT EXISTS `tbl_locations` (
  `location_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `location_tag` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `state_id` int(10) unsigned DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `head_id` int(10) unsigned DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `creator_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;