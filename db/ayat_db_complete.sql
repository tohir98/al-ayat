/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50524
Source Host           : localhost:3306
Source Database       : ayat

Target Server Type    : MYSQL
Target Server Version : 50524
File Encoding         : 65001

Date: 2014-06-13 07:59:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_account`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE `tbl_account` (
  `account_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(25) unsigned DEFAULT NULL,
  `balance` double DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  KEY `FK_tbl_account_1` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_account
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_account_loan`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account_loan`;
CREATE TABLE `tbl_account_loan` (
  `account_loan_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `loan_reg_id` int(25) unsigned DEFAULT NULL,
  `balance` double DEFAULT NULL,
  PRIMARY KEY (`account_loan_id`),
  KEY `FK_tbl_account_loan_1` (`loan_reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_account_loan
-- ----------------------------
INSERT INTO `tbl_account_loan` VALUES ('1', '0', '23883');
INSERT INTO `tbl_account_loan` VALUES ('2', '2', '175000');
INSERT INTO `tbl_account_loan` VALUES ('3', '0', '180000');
INSERT INTO `tbl_account_loan` VALUES ('4', '4', '180000');

-- ----------------------------
-- Table structure for `tbl_acct_type`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_acct_type`;
CREATE TABLE `tbl_acct_type` (
  `acct_type_id` int(25) NOT NULL AUTO_INCREMENT,
  `acct_type` varchar(50) DEFAULT NULL,
  `charge` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`acct_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_acct_type
-- ----------------------------
INSERT INTO `tbl_acct_type` VALUES ('1', 'TYPE A', '0.07', '2030-07-11 02:33:37');
INSERT INTO `tbl_acct_type` VALUES ('2', 'TYPE B', '0.08', '2030-07-11 02:33:25');
INSERT INTO `tbl_acct_type` VALUES ('3', 'TYPE C', '0.09', '2030-07-11 02:33:13');

-- ----------------------------
-- Table structure for `tbl_blk_amt`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_blk_amt`;
CREATE TABLE `tbl_blk_amt` (
  `blk_amt_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `loan_reg_id` int(25) unsigned DEFAULT NULL,
  `blk_amt` double DEFAULT NULL,
  `datetime` datetime NOT NULL,
  `acct_officer` varchar(150) NOT NULL,
  PRIMARY KEY (`blk_amt_id`),
  KEY `FK_tbl_blk_amt_1` (`loan_reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_blk_amt
-- ----------------------------
INSERT INTO `tbl_blk_amt` VALUES ('5', '1', '60000', '2019-08-11 04:27:25', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('6', '0', '50000', '2013-09-04 07:03:06', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('7', '0', '4684.6', '2013-09-04 07:05:02', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('8', '0', '60000', '2013-09-04 07:08:41', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('9', '0', '17695.4', '2013-09-04 07:10:27', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('10', '0', '4000', '2013-09-04 07:13:47', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('11', '0', '9019.8', '2013-09-04 07:15:33', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('12', '0', '469', '2013-09-04 07:18:47', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('13', '0', '5135.4', '2013-09-05 01:46:04', 'sysadmin administrator');
INSERT INTO `tbl_blk_amt` VALUES ('14', '1', '5135.4', '2013-09-05 01:50:26', 'sysadmin administrator');

-- ----------------------------
-- Table structure for `tbl_building_deposit`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_building_deposit`;
CREATE TABLE `tbl_building_deposit` (
  `deposit_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `acct_officer` varchar(100) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `dep_name` varchar(100) DEFAULT NULL,
  `dep_phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`deposit_id`),
  KEY `FK_tbl_deposit_1` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_building_deposit
-- ----------------------------
INSERT INTO `tbl_building_deposit` VALUES ('1', '1', '2150', 'sysadmin administrator', '2013-09-16 02:51:53', 'AJAYI TOSIN', null);
INSERT INTO `tbl_building_deposit` VALUES ('2', '2', '2550', 'sysadmin administrator', '2013-09-16 02:53:34', 'SHONUBI TAIWO', null);
INSERT INTO `tbl_building_deposit` VALUES ('3', '1', '600', 'sysadmin administrator', '2013-09-17 12:10:54', 'AJAYI TOSIN', null);
INSERT INTO `tbl_building_deposit` VALUES ('4', '1', '459', 'sysadmin administrator', '2013-09-19 12:18:51', 'jhdj', null);

-- ----------------------------
-- Table structure for `tbl_customer`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE `tbl_customer` (
  `customer_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(150) DEFAULT NULL,
  `last_name` varchar(150) DEFAULT NULL,
  `gender_id` int(25) NOT NULL,
  `marital_status_id` int(25) NOT NULL,
  `address` varchar(400) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `regional_state_id` int(10) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(150) NOT NULL,
  `acct_type_id` int(25) DEFAULT NULL,
  `open_date` datetime DEFAULT NULL,
  `acct_no` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`customer_id`),
  KEY `FK_tbl_customer_1` (`acct_type_id`),
  KEY `FK_tbl_customer_2` (`gender_id`),
  KEY `FK_tbl_customer_3` (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of tbl_customer
-- ----------------------------
INSERT INTO `tbl_customer` VALUES ('1', 'ajayi', 'tosin', '2', '1', 'ijebu north', 'Lagos', '0', '0809697757', '', '1', '2013-09-09 09:14:44', '2110010001');
INSERT INTO `tbl_customer` VALUES ('2', 'shonubi', 'taiwo', '2', '1', 'lagos nigeria', 'Ikeja', '0', '080', '', '2', '2013-09-10 07:02:24', '2110010002');
INSERT INTO `tbl_customer` VALUES ('3', 'lhidhi', 'jgubiu', '2', '2', 'opih8dsbu', 'poh', '0', '6543', '', '3', '2013-09-13 08:59:10', '2110010003');

-- ----------------------------
-- Table structure for `tbl_deposit`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_deposit`;
CREATE TABLE `tbl_deposit` (
  `deposit_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `acct_officer` varchar(100) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `dep_name` varchar(100) DEFAULT NULL,
  `dep_phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`deposit_id`),
  KEY `FK_tbl_deposit_1` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_deposit
-- ----------------------------
INSERT INTO `tbl_deposit` VALUES ('1', '1', '1000', 'sysadmin administrator', '2013-09-09 09:15:43', 'AJAYI TOSIN', null);
INSERT INTO `tbl_deposit` VALUES ('3', '2', '1000', 'sysadmin administrator', '2013-09-10 07:03:47', 'SHONUBI TAIWO', null);
INSERT INTO `tbl_deposit` VALUES ('5', '2', '1000', 'sysadmin administrator', '2013-09-16 02:53:17', 'SHONUBI TAIWO', null);
INSERT INTO `tbl_deposit` VALUES ('6', '1', '600', 'sysadmin administrator', '2013-09-16 04:40:07', 'AJAYI TOSIN', null);
INSERT INTO `tbl_deposit` VALUES ('7', '1', '1200', '', '2013-09-17 05:33:29', 'AJAYI TOSIN', null);
INSERT INTO `tbl_deposit` VALUES ('8', '2', '1100', '', '2013-09-17 05:33:36', 'SHONUBI TAIWO', null);
INSERT INTO `tbl_deposit` VALUES ('22', '2', '3000', 'sysadmin administrator', '2013-09-19 11:57:07', 'sule', null);
INSERT INTO `tbl_deposit` VALUES ('23', '1', '786655', 'sysadmin administrator', '2013-09-19 11:57:49', 'DELE', null);

-- ----------------------------
-- Table structure for `tbl_gender`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_gender`;
CREATE TABLE `tbl_gender` (
  `gender_id` int(25) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_gender
-- ----------------------------
INSERT INTO `tbl_gender` VALUES ('1', 'Male');
INSERT INTO `tbl_gender` VALUES ('2', 'Female');

-- ----------------------------
-- Table structure for `tbl_interest`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_interest`;
CREATE TABLE `tbl_interest` (
  `interest_id` int(25) NOT NULL AUTO_INCREMENT,
  `customer_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`interest_id`),
  KEY `FK_tbl_interest_1` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_interest
-- ----------------------------
INSERT INTO `tbl_interest` VALUES ('1', '9', '1440', '2030-07-11 04:51:44');
INSERT INTO `tbl_interest` VALUES ('2', '10', '680', '2030-07-11 08:39:05');
INSERT INTO `tbl_interest` VALUES ('3', '11', '630', '2012-08-11 11:39:41');
INSERT INTO `tbl_interest` VALUES ('4', '12', '2800', '2017-08-11 02:09:57');
INSERT INTO `tbl_interest` VALUES ('5', '13', '1680', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `tbl_loan_reg`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_loan_reg`;
CREATE TABLE `tbl_loan_reg` (
  `loan_reg_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `surname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `acct_no` char(10) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `approved_amt` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `acct_officer` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`loan_reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_loan_reg
-- ----------------------------
INSERT INTO `tbl_loan_reg` VALUES ('1', '', '', 'kjgjdkjd', '', '', '23883', '2013-09-16 09:25:59', 'sysadmin administrator');
INSERT INTO `tbl_loan_reg` VALUES ('2', 'ajayi', '', '2110010001', 'ijebu north', '0809697757', '250000', '2013-09-16 09:28:40', 'sysadmin administrator');
INSERT INTO `tbl_loan_reg` VALUES ('3', 'shonubi', 'taiwo', '2110010002', 'lagos nigeria', '080', '180000', '2013-09-20 04:01:36', 'sysadmin administrator');

-- ----------------------------
-- Table structure for `tbl_log`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE `tbl_log` (
  `log_id` int(25) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `operation` varchar(500) NOT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM AUTO_INCREMENT=463 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_log
-- ----------------------------
INSERT INTO `tbl_log` VALUES ('1', 'sysadmin administrator', 'User logs on', '2013/09/09 09:12:26');
INSERT INTO `tbl_log` VALUES ('2', 'sysadmin administrator', 'User logs out', '2013/09/09 09:13:18');
INSERT INTO `tbl_log` VALUES ('3', 'sysadmin administrator', 'User logs on', '2013/09/09 09:13:55');
INSERT INTO `tbl_log` VALUES ('4', 'sysadmin administrator', 'Account Editting page visited', '2013/09/09 09:14:16');
INSERT INTO `tbl_log` VALUES ('5', 'sysadmin administrator', 'Account Editting page visited', '2013/09/09 09:14:19');
INSERT INTO `tbl_log` VALUES ('6', 'sysadmin administrator', 'New Account page visited', '2013/09/09 09:14:20');
INSERT INTO `tbl_log` VALUES ('7', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:15:02');
INSERT INTO `tbl_log` VALUES ('8', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:15:02');
INSERT INTO `tbl_log` VALUES ('9', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:15:17');
INSERT INTO `tbl_log` VALUES ('10', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:15:17');
INSERT INTO `tbl_log` VALUES ('11', 'sysadmin administrator', 'Total Withdrawal page visited', '09/09/13 09:15:27');
INSERT INTO `tbl_log` VALUES ('12', 'sysadmin administrator', 'Total Deposit Page visited', '09/09/13 09:15:29');
INSERT INTO `tbl_log` VALUES ('13', 'sysadmin administrator', 'Deposit page visited', '09/09/2013 09:15:32');
INSERT INTO `tbl_log` VALUES ('14', 'sysadmin administrator', 'Deposit page visited', '09/09/2013 09:15:35');
INSERT INTO `tbl_log` VALUES ('15', 'sysadmin administrator', 'Total Deposit Page visited', '09/09/13 09:15:49');
INSERT INTO `tbl_log` VALUES ('16', 'sysadmin administrator', 'Total Withdrawal page visited', '09/09/13 09:15:54');
INSERT INTO `tbl_log` VALUES ('17', 'sysadmin administrator', 'Account Editting page visited', '2013/09/09 09:15:57');
INSERT INTO `tbl_log` VALUES ('18', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:16:05');
INSERT INTO `tbl_log` VALUES ('19', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:16:05');
INSERT INTO `tbl_log` VALUES ('20', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:17:24');
INSERT INTO `tbl_log` VALUES ('21', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:17:24');
INSERT INTO `tbl_log` VALUES ('22', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:17:42');
INSERT INTO `tbl_log` VALUES ('23', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:17:42');
INSERT INTO `tbl_log` VALUES ('24', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:17:46');
INSERT INTO `tbl_log` VALUES ('25', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:17:46');
INSERT INTO `tbl_log` VALUES ('26', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:18:00');
INSERT INTO `tbl_log` VALUES ('27', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:18:00');
INSERT INTO `tbl_log` VALUES ('28', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:22:54');
INSERT INTO `tbl_log` VALUES ('29', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:22:54');
INSERT INTO `tbl_log` VALUES ('30', 'sysadmin administrator', 'User logs on', '2013/09/09 09:22:58');
INSERT INTO `tbl_log` VALUES ('31', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:23:03');
INSERT INTO `tbl_log` VALUES ('32', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:23:03');
INSERT INTO `tbl_log` VALUES ('33', 'sysadmin administrator', 'Total Withdrawal page visited', '09/09/13 09:23:11');
INSERT INTO `tbl_log` VALUES ('34', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:23:30');
INSERT INTO `tbl_log` VALUES ('35', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:23:30');
INSERT INTO `tbl_log` VALUES ('36', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:23:47');
INSERT INTO `tbl_log` VALUES ('37', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:23:47');
INSERT INTO `tbl_log` VALUES ('38', 'sysadmin administrator', 'User logs on', '2013/09/09 09:24:29');
INSERT INTO `tbl_log` VALUES ('39', 'sysadmin administrator', 'Account Balance Page visited', '09/09/13 09:24:31');
INSERT INTO `tbl_log` VALUES ('40', 'sysadmin administrator', 'Account Balance Page visited', '09/09/13 09:24:36');
INSERT INTO `tbl_log` VALUES ('41', 'sysadmin administrator', 'Statement of account visited', '2013/09/09 09:24:56');
INSERT INTO `tbl_log` VALUES ('42', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:25:34');
INSERT INTO `tbl_log` VALUES ('43', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:25:34');
INSERT INTO `tbl_log` VALUES ('44', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:25:41');
INSERT INTO `tbl_log` VALUES ('45', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:25:41');
INSERT INTO `tbl_log` VALUES ('46', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:25:46');
INSERT INTO `tbl_log` VALUES ('47', 'sysadmin administrator', 'Account Withdrawal Page visited', '09/09/13 09:25:46');
INSERT INTO `tbl_log` VALUES ('48', 'sysadmin administrator', 'Total Withdrawal page visited', '09/09/13 09:25:52');
INSERT INTO `tbl_log` VALUES ('49', 'sysadmin administrator', 'User logs on', '2013/09/09 09:26:47');
INSERT INTO `tbl_log` VALUES ('50', 'sysadmin administrator', 'User logs on', '2013/09/09 09:26:59');
INSERT INTO `tbl_log` VALUES ('51', 'sysadmin administrator', 'User logs out', '2013/09/09 09:27:44');
INSERT INTO `tbl_log` VALUES ('52', 'sysadmin administrator', 'User logs on', '2013/09/09 09:37:43');
INSERT INTO `tbl_log` VALUES ('53', 'sysadmin administrator', 'Statement of account visited', '2013/09/09 09:37:50');
INSERT INTO `tbl_log` VALUES ('54', 'sysadmin administrator', 'Deposit page visited', '09/09/2013 09:38:04');
INSERT INTO `tbl_log` VALUES ('55', 'sysadmin administrator', 'Deposit page visited', '09/09/2013 09:38:14');
INSERT INTO `tbl_log` VALUES ('56', 'sysadmin administrator', 'Statement of account visited', '2013/09/09 09:38:33');
INSERT INTO `tbl_log` VALUES ('57', 'sysadmin administrator', 'User logs out', '2013/09/09 09:39:01');
INSERT INTO `tbl_log` VALUES ('58', 'sysadmin administrator', 'User logs on', '2013/09/10 07:00:29');
INSERT INTO `tbl_log` VALUES ('59', 'sysadmin administrator', 'New user registration page visited', '2013/09/10 07:00:39');
INSERT INTO `tbl_log` VALUES ('60', 'sysadmin administrator', 'User logs on', '2013/09/10 07:01:27');
INSERT INTO `tbl_log` VALUES ('61', 'sysadmin administrator', 'New user registration page visited', '2013/09/10 07:01:30');
INSERT INTO `tbl_log` VALUES ('62', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/10 07:01:43');
INSERT INTO `tbl_log` VALUES ('63', 'sysadmin administrator', 'New Account page visited', '2013/09/10 07:01:57');
INSERT INTO `tbl_log` VALUES ('64', 'sysadmin administrator', 'Deposit page visited', '10/09/2013 07:03:39');
INSERT INTO `tbl_log` VALUES ('65', 'sysadmin administrator', 'Deposit page visited', '10/09/2013 07:03:43');
INSERT INTO `tbl_log` VALUES ('66', 'sysadmin administrator', 'Total Deposit Page visited', '10/09/13 07:05:18');
INSERT INTO `tbl_log` VALUES ('67', 'sysadmin administrator', 'User logs on', '2013/09/10 07:11:27');
INSERT INTO `tbl_log` VALUES ('68', 'sysadmin administrator', 'Total Withdrawal page visited', '10/09/13 07:11:31');
INSERT INTO `tbl_log` VALUES ('69', 'sysadmin administrator', 'Statement of account visited', '2013/09/10 07:11:55');
INSERT INTO `tbl_log` VALUES ('70', 'sysadmin administrator', 'Statement of account visited', '2013/09/10 07:13:14');
INSERT INTO `tbl_log` VALUES ('71', 'sysadmin administrator', 'Statement of account visited', '2013/09/10 07:13:40');
INSERT INTO `tbl_log` VALUES ('72', 'sysadmin administrator', 'Statement of account visited', '2013/09/10 07:14:00');
INSERT INTO `tbl_log` VALUES ('73', 'sysadmin administrator', 'User logs out', '2013/09/10 07:14:06');
INSERT INTO `tbl_log` VALUES ('74', 'sysadmin administrator', 'User logs on', '2013/09/13 08:58:48');
INSERT INTO `tbl_log` VALUES ('75', 'sysadmin administrator', 'New Account page visited', '2013/09/13 08:58:53');
INSERT INTO `tbl_log` VALUES ('76', 'sysadmin administrator', 'User logs out', '2013/09/13 09:02:49');
INSERT INTO `tbl_log` VALUES ('77', 'sysadmin administrator', 'User logs on', '2013/09/15 01:15:44');
INSERT INTO `tbl_log` VALUES ('78', 'sysadmin administrator', 'Account Balance Page visited', '15/09/13 01:15:52');
INSERT INTO `tbl_log` VALUES ('79', 'sysadmin administrator', 'Account Balance Page visited', '15/09/13 01:15:56');
INSERT INTO `tbl_log` VALUES ('80', 'sysadmin administrator', 'Statement of account visited', '2013/09/15 01:16:04');
INSERT INTO `tbl_log` VALUES ('81', 'sysadmin administrator', 'Statement of account visited', '2013/09/15 01:33:44');
INSERT INTO `tbl_log` VALUES ('82', 'sysadmin administrator', 'Statement of account visited', '2013/09/15 01:34:56');
INSERT INTO `tbl_log` VALUES ('83', 'sysadmin administrator', 'User logs on', '2013/09/16 02:50:41');
INSERT INTO `tbl_log` VALUES ('84', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:51:39');
INSERT INTO `tbl_log` VALUES ('85', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:51:42');
INSERT INTO `tbl_log` VALUES ('86', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:52:04');
INSERT INTO `tbl_log` VALUES ('87', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:52:08');
INSERT INTO `tbl_log` VALUES ('88', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:53:05');
INSERT INTO `tbl_log` VALUES ('89', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:53:09');
INSERT INTO `tbl_log` VALUES ('90', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:53:22');
INSERT INTO `tbl_log` VALUES ('91', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 02:53:27');
INSERT INTO `tbl_log` VALUES ('92', 'sysadmin administrator', 'User logs on', '2013/09/16 02:59:14');
INSERT INTO `tbl_log` VALUES ('93', 'sysadmin administrator', 'User logs on', '2013/09/16 04:38:45');
INSERT INTO `tbl_log` VALUES ('94', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 04:39:54');
INSERT INTO `tbl_log` VALUES ('95', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 04:40:02');
INSERT INTO `tbl_log` VALUES ('96', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/16 09:10:04');
INSERT INTO `tbl_log` VALUES ('97', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/16 09:25:26');
INSERT INTO `tbl_log` VALUES ('98', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/16 09:27:31');
INSERT INTO `tbl_log` VALUES ('99', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/16 09:27:53');
INSERT INTO `tbl_log` VALUES ('100', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/16 09:28:08');
INSERT INTO `tbl_log` VALUES ('101', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 09:28:13');
INSERT INTO `tbl_log` VALUES ('102', 'sysadmin administrator', 'Deposit page visited', '16/09/2013 09:28:17');
INSERT INTO `tbl_log` VALUES ('103', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/16 09:29:47');
INSERT INTO `tbl_log` VALUES ('104', 'sysadmin administrator', 'User logs on', '2013/09/17 05:04:41');
INSERT INTO `tbl_log` VALUES ('105', 'sysadmin administrator', 'Account Deletion page visited', '17/09/13 05:06:27');
INSERT INTO `tbl_log` VALUES ('106', 'sysadmin administrator', 'Account Deletion page visited', '17/09/13 05:06:27');
INSERT INTO `tbl_log` VALUES ('107', '', 'Loan Repayment Page visited', '17/09/13 05:07:12');
INSERT INTO `tbl_log` VALUES ('108', '', 'Loan Repayment Page visited', '17/09/13 05:07:12');
INSERT INTO `tbl_log` VALUES ('109', '', 'Loan Repayment Page visited', '17/09/13 05:07:53');
INSERT INTO `tbl_log` VALUES ('110', '', 'Loan Repayment Page visited', '17/09/13 05:07:53');
INSERT INTO `tbl_log` VALUES ('111', '', 'User logs on', '2013/09/17 05:27:55');
INSERT INTO `tbl_log` VALUES ('112', '', 'Deposit page visited', '17/09/2013 05:33:20');
INSERT INTO `tbl_log` VALUES ('113', '', 'Deposit page visited', '17/09/2013 05:33:25');
INSERT INTO `tbl_log` VALUES ('114', '', 'Deposit page visited', '17/09/2013 05:33:33');
INSERT INTO `tbl_log` VALUES ('115', '', 'User logs on', '2013/09/17 05:45:42');
INSERT INTO `tbl_log` VALUES ('116', '', 'User logs on', '2013/09/17 05:46:25');
INSERT INTO `tbl_log` VALUES ('117', '', 'Statement of account visited', '2013/09/17 05:52:19');
INSERT INTO `tbl_log` VALUES ('118', '', 'Statement of account visited', '2013/09/17 05:52:19');
INSERT INTO `tbl_log` VALUES ('119', '', 'Loan Repayment Page visited', '17/09/13 08:51:12');
INSERT INTO `tbl_log` VALUES ('120', '', 'Loan Repayment Page visited', '17/09/13 08:51:12');
INSERT INTO `tbl_log` VALUES ('121', '', 'Loan Repayment Page visited', '17/09/13 08:54:39');
INSERT INTO `tbl_log` VALUES ('122', '', 'Loan Repayment Page visited', '17/09/13 08:54:39');
INSERT INTO `tbl_log` VALUES ('123', '', 'User logs on', '2013/09/17 09:10:30');
INSERT INTO `tbl_log` VALUES ('124', '', 'User logs on', '2013/09/17 09:11:23');
INSERT INTO `tbl_log` VALUES ('125', '', 'User logs on', '2013/09/17 09:14:59');
INSERT INTO `tbl_log` VALUES ('126', '', 'User logs on', '2013/09/17 09:15:32');
INSERT INTO `tbl_log` VALUES ('127', '', 'User logs on', '2013/09/17 09:16:41');
INSERT INTO `tbl_log` VALUES ('128', '', 'User logs on', '2013/09/17 09:16:42');
INSERT INTO `tbl_log` VALUES ('129', '', 'User logs on', '2013/09/17 09:16:43');
INSERT INTO `tbl_log` VALUES ('130', '', 'User logs on', '2013/09/17 09:16:44');
INSERT INTO `tbl_log` VALUES ('131', '', 'User logs on', '2013/09/17 09:16:54');
INSERT INTO `tbl_log` VALUES ('132', '', 'Statement of account visited', '2013/09/17 09:21:21');
INSERT INTO `tbl_log` VALUES ('133', '', 'Statement of account visited', '2013/09/17 09:23:47');
INSERT INTO `tbl_log` VALUES ('134', '', 'Statement of account visited', '2013/09/17 09:24:59');
INSERT INTO `tbl_log` VALUES ('135', '', 'Statement of account visited', '2013/09/17 09:25:43');
INSERT INTO `tbl_log` VALUES ('136', '', 'Statement of account visited', '2013/09/17 09:25:52');
INSERT INTO `tbl_log` VALUES ('137', '', 'Statement of account visited', '2013/09/17 09:30:04');
INSERT INTO `tbl_log` VALUES ('138', '', 'Statement of account visited', '2013/09/17 09:31:01');
INSERT INTO `tbl_log` VALUES ('139', '', 'Statement of account visited', '2013/09/17 09:32:01');
INSERT INTO `tbl_log` VALUES ('140', '', 'Statement of account visited', '2013/09/17 09:32:31');
INSERT INTO `tbl_log` VALUES ('141', '', 'Statement of account visited', '2013/09/17 09:37:37');
INSERT INTO `tbl_log` VALUES ('142', '', 'Statement of account visited', '2013/09/17 09:38:18');
INSERT INTO `tbl_log` VALUES ('143', '', 'Statement of account visited', '2013/09/17 09:45:42');
INSERT INTO `tbl_log` VALUES ('144', '', 'Statement of account visited', '2013/09/17 09:46:30');
INSERT INTO `tbl_log` VALUES ('145', '', 'Statement of account visited', '2013/09/17 09:50:43');
INSERT INTO `tbl_log` VALUES ('146', '', 'Statement of account visited', '2013/09/17 09:57:29');
INSERT INTO `tbl_log` VALUES ('147', '', 'User logs out', '2013/09/17 09:57:41');
INSERT INTO `tbl_log` VALUES ('148', 'sysadmin administrator', 'User logs on', '2013/09/18 08:20:57');
INSERT INTO `tbl_log` VALUES ('149', 'sysadmin administrator', 'Statement of account visited', '2013/09/18 08:21:05');
INSERT INTO `tbl_log` VALUES ('150', 'sysadmin administrator', 'User logs on', '2013/09/19 11:23:45');
INSERT INTO `tbl_log` VALUES ('151', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:23:57');
INSERT INTO `tbl_log` VALUES ('152', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:25:06');
INSERT INTO `tbl_log` VALUES ('153', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:25:56');
INSERT INTO `tbl_log` VALUES ('154', 'sysadmin administrator', 'User logs on', '2013/09/19 11:33:46');
INSERT INTO `tbl_log` VALUES ('155', 'sysadmin administrator', 'User logs on', '2013/09/19 11:33:47');
INSERT INTO `tbl_log` VALUES ('156', 'sysadmin administrator', 'User logs on', '2013/09/19 11:33:48');
INSERT INTO `tbl_log` VALUES ('157', 'sysadmin administrator', 'User logs on', '2013/09/19 11:33:50');
INSERT INTO `tbl_log` VALUES ('158', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:33:57');
INSERT INTO `tbl_log` VALUES ('159', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:34:00');
INSERT INTO `tbl_log` VALUES ('160', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:35:50');
INSERT INTO `tbl_log` VALUES ('161', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:41:48');
INSERT INTO `tbl_log` VALUES ('162', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:41:49');
INSERT INTO `tbl_log` VALUES ('163', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:04');
INSERT INTO `tbl_log` VALUES ('164', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('165', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('166', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('167', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('168', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('169', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('170', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('171', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('172', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('173', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('174', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:05');
INSERT INTO `tbl_log` VALUES ('175', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:06');
INSERT INTO `tbl_log` VALUES ('176', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:06');
INSERT INTO `tbl_log` VALUES ('177', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:06');
INSERT INTO `tbl_log` VALUES ('178', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:06');
INSERT INTO `tbl_log` VALUES ('179', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:07');
INSERT INTO `tbl_log` VALUES ('180', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:42:09');
INSERT INTO `tbl_log` VALUES ('181', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:09');
INSERT INTO `tbl_log` VALUES ('182', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('183', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('184', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('185', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('186', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('187', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('188', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('189', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('190', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('191', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:10');
INSERT INTO `tbl_log` VALUES ('192', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:11');
INSERT INTO `tbl_log` VALUES ('193', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:11');
INSERT INTO `tbl_log` VALUES ('194', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:11');
INSERT INTO `tbl_log` VALUES ('195', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:11');
INSERT INTO `tbl_log` VALUES ('196', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:11');
INSERT INTO `tbl_log` VALUES ('197', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:44:11');
INSERT INTO `tbl_log` VALUES ('198', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:01');
INSERT INTO `tbl_log` VALUES ('199', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:01');
INSERT INTO `tbl_log` VALUES ('200', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:01');
INSERT INTO `tbl_log` VALUES ('201', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:01');
INSERT INTO `tbl_log` VALUES ('202', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:01');
INSERT INTO `tbl_log` VALUES ('203', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:01');
INSERT INTO `tbl_log` VALUES ('204', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('205', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('206', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('207', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('208', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('209', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('210', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('211', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('212', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('213', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('214', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:02');
INSERT INTO `tbl_log` VALUES ('215', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:03');
INSERT INTO `tbl_log` VALUES ('216', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:03');
INSERT INTO `tbl_log` VALUES ('217', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:03');
INSERT INTO `tbl_log` VALUES ('218', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:03');
INSERT INTO `tbl_log` VALUES ('219', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:03');
INSERT INTO `tbl_log` VALUES ('220', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:04');
INSERT INTO `tbl_log` VALUES ('221', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:04');
INSERT INTO `tbl_log` VALUES ('222', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('223', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('224', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('225', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('226', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('227', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('228', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('229', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('230', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:05');
INSERT INTO `tbl_log` VALUES ('231', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:45:06');
INSERT INTO `tbl_log` VALUES ('232', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:19');
INSERT INTO `tbl_log` VALUES ('233', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:20');
INSERT INTO `tbl_log` VALUES ('234', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:20');
INSERT INTO `tbl_log` VALUES ('235', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:20');
INSERT INTO `tbl_log` VALUES ('236', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:20');
INSERT INTO `tbl_log` VALUES ('237', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('238', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('239', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('240', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('241', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('242', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('243', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:21');
INSERT INTO `tbl_log` VALUES ('244', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:24');
INSERT INTO `tbl_log` VALUES ('245', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:24');
INSERT INTO `tbl_log` VALUES ('246', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:24');
INSERT INTO `tbl_log` VALUES ('247', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:24');
INSERT INTO `tbl_log` VALUES ('248', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:25');
INSERT INTO `tbl_log` VALUES ('249', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:25');
INSERT INTO `tbl_log` VALUES ('250', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:25');
INSERT INTO `tbl_log` VALUES ('251', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 11:57:25');
INSERT INTO `tbl_log` VALUES ('252', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:01:55');
INSERT INTO `tbl_log` VALUES ('253', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:01:55');
INSERT INTO `tbl_log` VALUES ('254', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:01:55');
INSERT INTO `tbl_log` VALUES ('255', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:02:02');
INSERT INTO `tbl_log` VALUES ('256', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:02:05');
INSERT INTO `tbl_log` VALUES ('257', 'sysadmin administrator', 'Account Withdrawal Page visited', '19/09/13 12:05:44');
INSERT INTO `tbl_log` VALUES ('258', 'sysadmin administrator', 'Account Withdrawal Page visited', '19/09/13 12:05:44');
INSERT INTO `tbl_log` VALUES ('259', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:06:17');
INSERT INTO `tbl_log` VALUES ('260', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:06:17');
INSERT INTO `tbl_log` VALUES ('261', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:10:34');
INSERT INTO `tbl_log` VALUES ('262', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:10:34');
INSERT INTO `tbl_log` VALUES ('263', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:10:50');
INSERT INTO `tbl_log` VALUES ('264', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:11:33');
INSERT INTO `tbl_log` VALUES ('265', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:13:33');
INSERT INTO `tbl_log` VALUES ('266', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:13:33');
INSERT INTO `tbl_log` VALUES ('267', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:14:43');
INSERT INTO `tbl_log` VALUES ('268', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:14:43');
INSERT INTO `tbl_log` VALUES ('269', 'sysadmin administrator', 'Loan Repayment Page visited', '19/09/13 12:15:04');
INSERT INTO `tbl_log` VALUES ('270', 'sysadmin administrator', 'Loan Repayment Page visited', '19/09/13 12:15:04');
INSERT INTO `tbl_log` VALUES ('271', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:18:25');
INSERT INTO `tbl_log` VALUES ('272', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:18:25');
INSERT INTO `tbl_log` VALUES ('273', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:18:26');
INSERT INTO `tbl_log` VALUES ('274', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:18:26');
INSERT INTO `tbl_log` VALUES ('275', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:20:13');
INSERT INTO `tbl_log` VALUES ('276', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:20:13');
INSERT INTO `tbl_log` VALUES ('277', 'sysadmin administrator', 'User logs on', '2013/09/19 12:22:15');
INSERT INTO `tbl_log` VALUES ('278', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:22:18');
INSERT INTO `tbl_log` VALUES ('279', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:22:18');
INSERT INTO `tbl_log` VALUES ('280', 'sysadmin administrator', 'User logs on', '2013/09/19 12:24:14');
INSERT INTO `tbl_log` VALUES ('281', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:24:17');
INSERT INTO `tbl_log` VALUES ('282', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:24:17');
INSERT INTO `tbl_log` VALUES ('283', 'sysadmin administrator', 'User logs on', '2013/09/19 12:25:21');
INSERT INTO `tbl_log` VALUES ('284', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:25:22');
INSERT INTO `tbl_log` VALUES ('285', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 12:25:22');
INSERT INTO `tbl_log` VALUES ('286', 'sysadmin administrator', 'User logs on', '2013/09/19 05:31:51');
INSERT INTO `tbl_log` VALUES ('287', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:31:54');
INSERT INTO `tbl_log` VALUES ('288', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:31:54');
INSERT INTO `tbl_log` VALUES ('289', 'sysadmin administrator', 'User logs on', '2013/09/19 05:33:21');
INSERT INTO `tbl_log` VALUES ('290', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:33:23');
INSERT INTO `tbl_log` VALUES ('291', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:33:23');
INSERT INTO `tbl_log` VALUES ('292', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:33:48');
INSERT INTO `tbl_log` VALUES ('293', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:33:48');
INSERT INTO `tbl_log` VALUES ('294', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:35:08');
INSERT INTO `tbl_log` VALUES ('295', 'sysadmin administrator', 'Deposit page visited', '19/09/2013 05:35:08');
INSERT INTO `tbl_log` VALUES ('296', 'sysadmin administrator', 'User logs on', '2013/09/19 05:36:26');
INSERT INTO `tbl_log` VALUES ('297', 'sysadmin administrator', 'Account Balance Page visited', '19/09/13 05:36:28');
INSERT INTO `tbl_log` VALUES ('298', 'sysadmin administrator', 'Account Balance Page visited', '19/09/13 05:36:30');
INSERT INTO `tbl_log` VALUES ('299', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 05:36:40');
INSERT INTO `tbl_log` VALUES ('300', 'sysadmin administrator', 'User logs on', '2013/09/19 05:36:55');
INSERT INTO `tbl_log` VALUES ('301', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 05:37:00');
INSERT INTO `tbl_log` VALUES ('302', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 05:38:20');
INSERT INTO `tbl_log` VALUES ('303', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 05:39:19');
INSERT INTO `tbl_log` VALUES ('304', 'sysadmin administrator', 'User logs on', '2013/09/19 08:33:11');
INSERT INTO `tbl_log` VALUES ('305', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:37:23');
INSERT INTO `tbl_log` VALUES ('306', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:37:30');
INSERT INTO `tbl_log` VALUES ('307', 'sysadmin administrator', 'User logs on', '2013/09/19 08:39:14');
INSERT INTO `tbl_log` VALUES ('308', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:39:41');
INSERT INTO `tbl_log` VALUES ('309', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:42:19');
INSERT INTO `tbl_log` VALUES ('310', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:42:58');
INSERT INTO `tbl_log` VALUES ('311', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:45:52');
INSERT INTO `tbl_log` VALUES ('312', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:50:30');
INSERT INTO `tbl_log` VALUES ('313', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:54:05');
INSERT INTO `tbl_log` VALUES ('314', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:55:48');
INSERT INTO `tbl_log` VALUES ('315', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:57:42');
INSERT INTO `tbl_log` VALUES ('316', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 08:58:11');
INSERT INTO `tbl_log` VALUES ('317', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:14:59');
INSERT INTO `tbl_log` VALUES ('318', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:15:34');
INSERT INTO `tbl_log` VALUES ('319', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:16:58');
INSERT INTO `tbl_log` VALUES ('320', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:19:52');
INSERT INTO `tbl_log` VALUES ('321', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:20:17');
INSERT INTO `tbl_log` VALUES ('322', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:20:39');
INSERT INTO `tbl_log` VALUES ('323', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:21:10');
INSERT INTO `tbl_log` VALUES ('324', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:21:41');
INSERT INTO `tbl_log` VALUES ('325', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:21:51');
INSERT INTO `tbl_log` VALUES ('326', 'sysadmin administrator', 'Statement of account visited', '2013/09/19 09:21:58');
INSERT INTO `tbl_log` VALUES ('327', 'sysadmin administrator', 'User logs out', '2013/09/19 09:22:58');
INSERT INTO `tbl_log` VALUES ('328', 'sysadmin administrator', 'User logs on', '2013/09/20 04:01:08');
INSERT INTO `tbl_log` VALUES ('329', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:01:16');
INSERT INTO `tbl_log` VALUES ('330', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:01:43');
INSERT INTO `tbl_log` VALUES ('331', 'sysadmin administrator', 'User logs on', '2013/09/20 04:24:48');
INSERT INTO `tbl_log` VALUES ('332', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:24:54');
INSERT INTO `tbl_log` VALUES ('333', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:26:57');
INSERT INTO `tbl_log` VALUES ('334', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:27:10');
INSERT INTO `tbl_log` VALUES ('335', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:27:16');
INSERT INTO `tbl_log` VALUES ('336', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:02');
INSERT INTO `tbl_log` VALUES ('337', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:09');
INSERT INTO `tbl_log` VALUES ('338', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:10');
INSERT INTO `tbl_log` VALUES ('339', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:11');
INSERT INTO `tbl_log` VALUES ('340', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('341', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('342', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('343', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('344', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('345', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('346', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('347', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('348', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:12');
INSERT INTO `tbl_log` VALUES ('349', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:27');
INSERT INTO `tbl_log` VALUES ('350', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:28');
INSERT INTO `tbl_log` VALUES ('351', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:28');
INSERT INTO `tbl_log` VALUES ('352', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:28');
INSERT INTO `tbl_log` VALUES ('353', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:28');
INSERT INTO `tbl_log` VALUES ('354', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('355', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('356', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('357', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('358', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('359', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('360', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:29');
INSERT INTO `tbl_log` VALUES ('361', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:42');
INSERT INTO `tbl_log` VALUES ('362', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:29:54');
INSERT INTO `tbl_log` VALUES ('363', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:30:03');
INSERT INTO `tbl_log` VALUES ('364', 'sysadmin administrator', 'Total Deposit Page visited', '20/09/13 04:30:11');
INSERT INTO `tbl_log` VALUES ('365', 'sysadmin administrator', 'Total Deposit Page visited', '20/09/13 04:30:11');
INSERT INTO `tbl_log` VALUES ('366', 'sysadmin administrator', 'Total Deposit Page visited', '20/09/13 04:30:20');
INSERT INTO `tbl_log` VALUES ('367', 'sysadmin administrator', 'Total Withdrawal page visited', '20/09/13 04:30:21');
INSERT INTO `tbl_log` VALUES ('368', 'sysadmin administrator', 'Loan Reg. Page visited', '2013/09/20 04:31:28');
INSERT INTO `tbl_log` VALUES ('369', 'sysadmin administrator', 'User logs out', '2013/09/20 04:31:35');
INSERT INTO `tbl_log` VALUES ('370', 'sysadmin administrator', 'User logs on', '2013/09/23 09:38:11');
INSERT INTO `tbl_log` VALUES ('371', 'sysadmin administrator', 'Account Deletion page visited', '23/09/13 09:38:26');
INSERT INTO `tbl_log` VALUES ('372', 'sysadmin administrator', 'Account Deletion page visited', '23/09/13 09:38:27');
INSERT INTO `tbl_log` VALUES ('373', 'sysadmin administrator', 'Account Editting page visited', '2013/09/23 09:38:29');
INSERT INTO `tbl_log` VALUES ('374', 'sysadmin administrator', 'Statement of account visited', '2013/09/23 09:38:37');
INSERT INTO `tbl_log` VALUES ('375', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 09:40:02');
INSERT INTO `tbl_log` VALUES ('376', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:06:54');
INSERT INTO `tbl_log` VALUES ('377', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:09:06');
INSERT INTO `tbl_log` VALUES ('378', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:16:57');
INSERT INTO `tbl_log` VALUES ('379', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:24:23');
INSERT INTO `tbl_log` VALUES ('380', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:25:38');
INSERT INTO `tbl_log` VALUES ('381', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:25:54');
INSERT INTO `tbl_log` VALUES ('382', 'sysadmin administrator', 'Account Editting page visited', '2013/09/23 10:32:29');
INSERT INTO `tbl_log` VALUES ('383', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:37:41');
INSERT INTO `tbl_log` VALUES ('384', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:38:20');
INSERT INTO `tbl_log` VALUES ('385', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:38:48');
INSERT INTO `tbl_log` VALUES ('386', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:38:51');
INSERT INTO `tbl_log` VALUES ('387', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('388', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('389', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('390', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('391', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('392', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('393', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:40:17');
INSERT INTO `tbl_log` VALUES ('394', 'sysadmin administrator', 'User logs on', '2013/09/23 10:41:34');
INSERT INTO `tbl_log` VALUES ('395', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:41:36');
INSERT INTO `tbl_log` VALUES ('396', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:21');
INSERT INTO `tbl_log` VALUES ('397', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:23');
INSERT INTO `tbl_log` VALUES ('398', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:23');
INSERT INTO `tbl_log` VALUES ('399', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:24');
INSERT INTO `tbl_log` VALUES ('400', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:24');
INSERT INTO `tbl_log` VALUES ('401', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:49');
INSERT INTO `tbl_log` VALUES ('402', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:51');
INSERT INTO `tbl_log` VALUES ('403', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:52');
INSERT INTO `tbl_log` VALUES ('404', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 10:42:52');
INSERT INTO `tbl_log` VALUES ('405', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:07:44');
INSERT INTO `tbl_log` VALUES ('406', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:07:45');
INSERT INTO `tbl_log` VALUES ('407', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:56');
INSERT INTO `tbl_log` VALUES ('408', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:57');
INSERT INTO `tbl_log` VALUES ('409', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:57');
INSERT INTO `tbl_log` VALUES ('410', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:58');
INSERT INTO `tbl_log` VALUES ('411', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:58');
INSERT INTO `tbl_log` VALUES ('412', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:58');
INSERT INTO `tbl_log` VALUES ('413', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:58');
INSERT INTO `tbl_log` VALUES ('414', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:58');
INSERT INTO `tbl_log` VALUES ('415', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:58');
INSERT INTO `tbl_log` VALUES ('416', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:59');
INSERT INTO `tbl_log` VALUES ('417', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:59');
INSERT INTO `tbl_log` VALUES ('418', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:59');
INSERT INTO `tbl_log` VALUES ('419', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:08:59');
INSERT INTO `tbl_log` VALUES ('420', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:09:00');
INSERT INTO `tbl_log` VALUES ('421', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:09:02');
INSERT INTO `tbl_log` VALUES ('422', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:10:18');
INSERT INTO `tbl_log` VALUES ('423', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:11:52');
INSERT INTO `tbl_log` VALUES ('424', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:11:54');
INSERT INTO `tbl_log` VALUES ('425', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:12:20');
INSERT INTO `tbl_log` VALUES ('426', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:24:47');
INSERT INTO `tbl_log` VALUES ('427', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:29:53');
INSERT INTO `tbl_log` VALUES ('428', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:30:07');
INSERT INTO `tbl_log` VALUES ('429', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:30:09');
INSERT INTO `tbl_log` VALUES ('430', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:30:10');
INSERT INTO `tbl_log` VALUES ('431', 'sysadmin administrator', 'Total Withdrawal page visited', '23/09/13 12:30:13');
INSERT INTO `tbl_log` VALUES ('432', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:30:16');
INSERT INTO `tbl_log` VALUES ('433', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:30:45');
INSERT INTO `tbl_log` VALUES ('434', 'sysadmin administrator', 'Total Deposit Page visited', '23/09/13 12:31:24');
INSERT INTO `tbl_log` VALUES ('435', 'sysadmin administrator', 'User logs out', '2013/09/24 08:51:43');
INSERT INTO `tbl_log` VALUES ('436', 'sysadmin administrator', 'User logs on', '2014/06/01 08:05:50');
INSERT INTO `tbl_log` VALUES ('437', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:07');
INSERT INTO `tbl_log` VALUES ('438', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:08');
INSERT INTO `tbl_log` VALUES ('439', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:09');
INSERT INTO `tbl_log` VALUES ('440', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:10');
INSERT INTO `tbl_log` VALUES ('441', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:11');
INSERT INTO `tbl_log` VALUES ('442', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:12');
INSERT INTO `tbl_log` VALUES ('443', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:13');
INSERT INTO `tbl_log` VALUES ('444', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:14');
INSERT INTO `tbl_log` VALUES ('445', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:15');
INSERT INTO `tbl_log` VALUES ('446', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:16');
INSERT INTO `tbl_log` VALUES ('447', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:17');
INSERT INTO `tbl_log` VALUES ('448', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:18');
INSERT INTO `tbl_log` VALUES ('449', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:19');
INSERT INTO `tbl_log` VALUES ('450', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:20');
INSERT INTO `tbl_log` VALUES ('451', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:21');
INSERT INTO `tbl_log` VALUES ('452', 'sysadmin administrator', 'New Account page visited', '2014/06/01 08:24:22');
INSERT INTO `tbl_log` VALUES ('453', 'sysadmin administrator', 'Account Deletion page visited', '01/06/14 08:24:23');
INSERT INTO `tbl_log` VALUES ('454', 'sysadmin administrator', 'Account Deletion page visited', '01/06/14 08:24:25');
INSERT INTO `tbl_log` VALUES ('455', 'sysadmin administrator', 'Account Editting page visited', '2014/06/01 08:24:44');
INSERT INTO `tbl_log` VALUES ('456', 'sysadmin administrator', 'Total Deposit Page visited', '01/06/14 08:27:11');
INSERT INTO `tbl_log` VALUES ('457', 'sysadmin administrator', 'Total Deposit Page visited', '01/06/14 08:27:14');
INSERT INTO `tbl_log` VALUES ('458', 'sysadmin administrator', 'Total Withdrawal page visited', '01/06/14 08:27:17');
INSERT INTO `tbl_log` VALUES ('459', 'sysadmin administrator', 'Deposit page visited', '01/06/2014 08:27:21');
INSERT INTO `tbl_log` VALUES ('460', 'sysadmin administrator', 'Deposit page visited', '01/06/2014 08:27:22');
INSERT INTO `tbl_log` VALUES ('461', 'sysadmin administrator', 'User logs out', '2014/06/01 11:08:33');
INSERT INTO `tbl_log` VALUES ('462', 'sysadmin administrator', 'User logs out', '2014/06/01 11:09:51');

-- ----------------------------
-- Table structure for `tbl_marital_status`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_marital_status`;
CREATE TABLE `tbl_marital_status` (
  `marital_status_id` int(25) NOT NULL AUTO_INCREMENT,
  `marital_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`marital_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_marital_status
-- ----------------------------
INSERT INTO `tbl_marital_status` VALUES ('1', 'Single');
INSERT INTO `tbl_marital_status` VALUES ('2', 'Married');

-- ----------------------------
-- Table structure for `tbl_modules`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_modules`;
CREATE TABLE `tbl_modules` (
  `id_module` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_group` int(255) unsigned NOT NULL,
  `status` int(255) DEFAULT '1',
  PRIMARY KEY (`id_module`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_modules
-- ----------------------------
INSERT INTO `tbl_modules` VALUES ('1', 'Manage Users', 'users', '1', '1');
INSERT INTO `tbl_modules` VALUES ('2', 'Members', 'member', '3', '1');
INSERT INTO `tbl_modules` VALUES ('3', 'Account', 'accounts', '2', '1');
INSERT INTO `tbl_modules` VALUES ('4', 'Transactions', 'transactions', '2', '1');
INSERT INTO `tbl_modules` VALUES ('5', 'Loans', 'loans', '3', '1');
INSERT INTO `tbl_modules` VALUES ('6', 'Account Settings', 'account_settings', '4', '1');
INSERT INTO `tbl_modules` VALUES ('7', 'Access Control', 'access_control', '4', '1');

-- ----------------------------
-- Table structure for `tbl_module_group`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_module_group`;
CREATE TABLE `tbl_module_group` (
  `id_group` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `id_sort` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_module_group
-- ----------------------------
INSERT INTO `tbl_module_group` VALUES ('1', 'Manage User', '2');
INSERT INTO `tbl_module_group` VALUES ('2', 'Members', '3');
INSERT INTO `tbl_module_group` VALUES ('3', 'Transactions', '1');
INSERT INTO `tbl_module_group` VALUES ('4', 'Account Settings', '4');

-- ----------------------------
-- Table structure for `tbl_module_perms`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_module_perms`;
CREATE TABLE `tbl_module_perms` (
  `id_perm` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `id_module` int(255) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL,
  `id_string` varchar(255) NOT NULL,
  `id_owner` int(255) unsigned NOT NULL,
  `in_menu` int(1) unsigned NOT NULL,
  `status` int(255) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_perm`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_module_perms
-- ----------------------------
INSERT INTO `tbl_module_perms` VALUES ('1', '1', 'Users', 'users', '1', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('2', '1', 'User Type', 'user_type', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('32', '9', 'Account Types', 'account_types', '4', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('33', '9', 'Delete Events', 'delete_events', '4', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('34', '6', 'Admin Profile Settings', 'admin_profile_settings', '7', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('35', '6', 'Company Profile Settings', 'company_profile_settings', '7', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('36', '5', 'My Tasks', 'my_tasks', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('39', '5', 'Edit Personal Profile', 'edit_profile_personal_info', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('46', '6', 'View Landing Page Settings', 'view_landing_page_settings', '7', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('47', '6', 'Landing Page Settings', 'landing_page_settings', '7', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('48', '4', 'View Live Jobs', 'view_live_jobs', '10', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('49', '4', 'Post Job', 'post_job', '10', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('50', '4', 'Edit Live Job Status', 'edit_live_job_status', '10', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('51', '4', 'Edit Job', 'edit_job', '10', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('52', '4', 'Watchlist', 'watchlist', '10', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('53', '4', 'Search Database', 'search_database', '10', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('54', '2', 'Vacation Entitlements', 'allocate_vacation', '5', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('55', '3', 'Appraisal Settings', 'appraisal_settings', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('56', '5', 'Change Task Status', 'change_task_status', '6', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('57', '10', 'Manage Payroll', 'payroll', '11', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('58', '10', 'Payroll Settings', 'settings', '11', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('59', '10', 'Paid Employees', 'paid_employee', '11', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('60', '10', 'Add To Payroll', 'unpaid_employee', '11', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('61', '10', 'My Payslip', 'my_payslip', '11', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('62', '10', 'Approve Payroll Process', 'approve_payroll_process', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('63', '10', 'Edit Payroll Currency', 'edit_payroll_currency', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('64', '10', 'Payroll History', 'history', '11', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('65', '10', 'List Payroll History', 'history', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('66', '10', 'Search Payroll History', 'search_payroll_history', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('67', '10', 'View Payroll History Details', 'view_payroll_history_details', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('68', '10', 'Run Payroll', 'start_payroll', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('69', '10', 'Paid Employee', 'paid_employee', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('70', '10', 'Select Employee To Process Payroll', 'select_employee_to_process_payroll', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('71', '10', 'Edit Payslip', 'edit_payslip', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('72', '10', 'Cancel Payroll Process', 'cancel_payroll_process', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('73', '10', 'Initiate Payment', 'initiate_payment', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('74', '10', 'Manage Payroll', 'payroll', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('75', '10', 'Add To Payroll', 'unpaid_employee', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('76', '10', 'View Employee Payslip', 'view_employee_payslip', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('77', '10', 'Edit Employee Payslip', 'edit_employee_payslip', '11', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('78', '9', 'Events Calendar', 'view_events_calendar', '4', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('79', '3', 'Appraisal Forms', 'appraisal_form', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('80', '2', 'Pending Approvals', 'pending_approvals', '5', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('81', '2', 'View Summary', 'view_summary', '5', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('82', '2', 'Add Holiday', 'add_holiday', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('83', '2', 'Edit Holiday', 'edit_holiday', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('84', '2', 'Delete Holiday', 'delete_holiday', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('85', '2', 'Add Vacation Type', 'add_vacation_type', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('86', '2', 'Delete Vacation Type', 'delete_vacation_type', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('87', '2', 'Edit Vacation Type', 'edit_vacation_type', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('88', '2', 'Make Vacation Request', 'make_vacation_request', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('89', '2', 'Approve Vacation Request', 'approve_vacation_request', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('90', '2', 'Decline Make Vacation Request', 'decline_vacation_request', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('91', '11', 'View Access Control', 'view_access_control', '12', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('92', '12', 'Issue New Query', 'issue_new_query', '5', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('93', '12', 'My Queries', 'my_queries', '5', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('94', '12', 'Query Records', 'query_records', '5', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('95', '12', 'Query', 'query', '5', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('96', '3', 'Appraisal Sections', 'view_appraisal_section', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('97', '3', 'Add Appraisal Section', 'add_appraisal_section', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('98', '3', 'Edit Appraisal Section', 'edit_appraisal_section', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('99', '3', 'Delete Appraisal Section', 'delete_appraisal_section', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('100', '13', 'Clients', 'clients', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('101', '13', 'Add New Client', 'add_new_client', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('102', '13', 'Account History', 'account_history', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('103', '1', 'Edit Benefits', 'edit_benefits', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('104', '1', 'Edit Resume/Credentials', 'edit_credentials', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('105', '1', 'Edit Work Profile', 'edit_profile_work_info', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('106', '1', 'Edit Bank details', 'edit_bank_details', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('107', '1', 'Edit Emergency contacts', 'edit_profile_emergency_contacts', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('108', '1', 'Edit Profile picture', 'edit_profile_picture', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('109', '5', 'My Profile', 'my_profile', '3', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('111', '5', 'View My Profile Picture', 'view_my_profile_picture', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('112', '5', 'View My Profile Work info', 'view_my_profile_work_info', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('113', '5', 'View My Profile Personal info', 'view_my_profile_personal_info', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('114', '5', 'View My Emergency Contacts', 'view_my_emergency_contacts', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('115', '5', 'View My Bank Details', 'view_my_bank_details', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('116', '5', 'View My Benefits Details', 'view_my_benefits_details', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('117', '5', 'Edit My profile Picture', 'edit_my_profile_picture', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('119', '5', 'Edit My Profile Personal info', 'edit_my_profile_personal_info', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('120', '5', 'Edit My Emergency Contacts', 'edit_my_emergency_contacts', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('121', '5', 'Edit My Bank Details', 'edit_my_bank_details', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('122', '5', 'Edit My Benefits Details', 'edit_my_benefits_details', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('123', '13', 'Edit HR Client Access', 'edit_hr_client_access', '3', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('125', '1', 'Request Employee Profile Picture', 'request_profile_picture', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('126', '1', 'Request Employee Personal Profile', 'request_profile_personal_info', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('127', '1', 'Request Employee Resume / Credentials', 'request_credentials', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('128', '1', 'Request Employee Emergency Contacts', 'request_emergency_contacts', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('129', '1', 'Request Employee Bank Details', 'request_bank_details', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('130', '1', 'Request Employee Benefits Details', 'request_benefits', '6', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('131', '1', 'Organogram', 'organogram', '1', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('132', '14', 'Device Locations', 'device_location', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('133', '14', 'Current Employees', 'current_employees', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('135', '14', 'Today\'s Attendance', 'today', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('136', '14', 'Attendance Records', 'records', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('137', '14', 'Settings', 'settings', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('138', '1', 'View Employee Records', 'view_records', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('139', '1', 'Change status', 'change_status', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('140', '15', 'Compensation', 'employees', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('141', '15', 'Add Employee', 'add_compensation', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('142', '15', 'Pay Structure View', 'view_pay_structure', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('143', '15', 'Add Pay Structure', 'add_pay_structure', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('144', '15', 'Edit Pay Structure', 'edit_pay_structure', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('145', '15', 'Delete Pay Structure', 'delete_pay_structure', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('146', '15', 'Add Compensation', 'add_compensation', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('147', '15', 'Edit Compensation', 'edit_compensation', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('148', '15', 'Delete Compensation', 'delete_compensation', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('149', '15', 'View Compensation', 'view_compensation', '1', '0', '0');
INSERT INTO `tbl_module_perms` VALUES ('150', '15', 'Pay Structure', 'pay_structure', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('151', '15', 'Compensation Settings', 'settings', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('152', '16', 'My Loan', 'my_loan', '1', '1', '0');
INSERT INTO `tbl_module_perms` VALUES ('153', '16', 'Pending Approval', 'pending_approval', '1', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('154', '16', 'Loan Settiings', 'loan_settings', '1', '1', '1');
INSERT INTO `tbl_module_perms` VALUES ('155', '14', 'Delete Setting', 'delete_setting', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('156', '14', 'Edit Setting', 'edit_setting', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('157', '14', 'Add New Record', 'add_new_record', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('158', '14', 'Delete Shift', 'delete_shift', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('159', '14', 'Edit Shift', 'edit_shift', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('160', '14', 'Create New Shift', 'create_new_shift', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('161', '14', 'View Employee Attendance Records', 'employee_attendance_records', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('162', '14', 'Edit Employee Shift', 'edit_employee_shift', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('163', '14', 'Refresh Attendance', 'refresh_attendance', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('164', '14', 'Remove Employee', 'remove_employee', '1', '0', '1');
INSERT INTO `tbl_module_perms` VALUES ('165', '14', 'Schedule', 'schedule', '1', '1', '0');

-- ----------------------------
-- Table structure for `tbl_period`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_period`;
CREATE TABLE `tbl_period` (
  `period_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `period` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`period_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_period
-- ----------------------------
INSERT INTO `tbl_period` VALUES ('1', '1 Month');
INSERT INTO `tbl_period` VALUES ('2', '2 Month');
INSERT INTO `tbl_period` VALUES ('3', '3 Month');
INSERT INTO `tbl_period` VALUES ('4', '4 Month');
INSERT INTO `tbl_period` VALUES ('5', '5 Month');
INSERT INTO `tbl_period` VALUES ('6', '6 Month');

-- ----------------------------
-- Table structure for `tbl_poc`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_poc`;
CREATE TABLE `tbl_poc` (
  `poc_id` int(25) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(150) DEFAULT NULL,
  `sector` varchar(150) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `start_date` varchar(50) DEFAULT NULL,
  `end_date` varchar(50) DEFAULT NULL,
  `attachment` varchar(150) DEFAULT NULL,
  `service_type` varchar(150) DEFAULT NULL,
  `details` varchar(250) DEFAULT NULL,
  `tit` varchar(150) DEFAULT NULL,
  `crm` varchar(150) DEFAULT NULL,
  `enter` varchar(150) DEFAULT NULL,
  `date_added` varchar(50) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `close_date` varchar(45) DEFAULT NULL,
  `remark` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`poc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_poc
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_rate`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_rate`;
CREATE TABLE `tbl_rate` (
  `rate_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `rate` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`rate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_rate
-- ----------------------------
INSERT INTO `tbl_rate` VALUES ('1', '0%');
INSERT INTO `tbl_rate` VALUES ('2', '1%');
INSERT INTO `tbl_rate` VALUES ('3', '2%');
INSERT INTO `tbl_rate` VALUES ('4', '3%');
INSERT INTO `tbl_rate` VALUES ('5', '4%');
INSERT INTO `tbl_rate` VALUES ('6', '5%');
INSERT INTO `tbl_rate` VALUES ('7', '6%');
INSERT INTO `tbl_rate` VALUES ('8', '7%');

-- ----------------------------
-- Table structure for `tbl_regional_states`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_regional_states`;
CREATE TABLE `tbl_regional_states` (
  `StateID` int(10) NOT NULL,
  `StateName` varchar(200) NOT NULL,
  `StateCapital` varchar(50) DEFAULT NULL,
  `StateCode` char(10) DEFAULT NULL,
  `Deleted` varchar(50) DEFAULT NULL,
  `DeletedBy` varchar(30) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_regional_states
-- ----------------------------
INSERT INTO `tbl_regional_states` VALUES ('1', 'Abia', '', 'AB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('2', 'Adamawa', '', 'AD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('3', 'Akwa Ibom', '', 'AK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('4', 'Anambra', '', 'AN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('5', 'Bauchi', '', 'BA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('7', 'Benue', '', 'BN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('8', 'Borno', '', 'BO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('6', 'Bayelsa', '', 'BY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('9', 'Cross Rivers', '', 'CR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('10', 'Delta', '', 'DT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('11', 'Ebonyi', '', 'EB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('12', 'Edo', '', 'ED', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('13', 'Ekiti', '', 'EK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('14', 'Enugu', '', 'EN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('15', 'FCT', '', 'FC', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('16', 'Gombe', '', 'GM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('17', 'Imo', '', 'IM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('18', 'Jigawa', '', 'JG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('22', 'Kebbi', '', 'KB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('19', 'Kaduna', '', 'KD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('23', 'Kogi', '', 'KG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('20', 'Kano', '', 'KN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('21', 'Katsina', '', 'KT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('24', 'Kwara', '', 'KW', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('25', 'Lagos', '', 'LA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('27', 'Niger', '', 'NG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('26', 'Nassarawa', '', 'NS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('29', 'Ondo', '', 'OD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('28', 'Ogun', '', 'OG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('30', 'Osun', '', 'OS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('31', 'Oyo', '', 'OY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('32', 'Plateau', '', 'PL', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('33', 'Rivers', '', 'RV', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('34', 'Sokoto', '', 'SO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('35', 'Taraba', '', 'TR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('36', 'Yobe', '', 'YB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_regional_states` VALUES ('37', 'Zamfara', '', 'ZM', '', '', '2008-08-02 18:55:22');

-- ----------------------------
-- Table structure for `tbl_repayment`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_repayment`;
CREATE TABLE `tbl_repayment` (
  `repayment_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `loan_reg_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `acct_officer` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`repayment_id`),
  KEY `FK_tbl_repayment_1` (`loan_reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_repayment
-- ----------------------------
INSERT INTO `tbl_repayment` VALUES ('1', '2', '50000', '2013-09-17 05:07:25', '');
INSERT INTO `tbl_repayment` VALUES ('2', '2', '25000', '2013-09-17 08:54:45', '');

-- ----------------------------
-- Table structure for `tbl_role`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE `tbl_role` (
  `role_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(150) DEFAULT NULL,
  `Password` varchar(50) DEFAULT NULL,
  `Firstname` varchar(100) DEFAULT NULL,
  `Lastname` varchar(100) DEFAULT NULL,
  `user_type_id` int(25) unsigned DEFAULT NULL,
  `islocked` varchar(5) DEFAULT 'no',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`role_id`),
  KEY `FK_tbl_role_1` (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_role
-- ----------------------------
INSERT INTO `tbl_role` VALUES ('7', 'sa@aol.com', 'admin', 'Babatunde', 'Fashola', '2', 'no', null);
INSERT INTO `tbl_role` VALUES ('8', 'thohiru.omoloye@talentbase.ng', '0d63031864eaeeab8baf66bee4e9c3b9', 'sade', 'samuel', '3', 'no', null);

-- ----------------------------
-- Table structure for `tbl_scheme`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_scheme`;
CREATE TABLE `tbl_scheme` (
  `scheme_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `scheme` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`scheme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_scheme
-- ----------------------------
INSERT INTO `tbl_scheme` VALUES ('1', 'Soft Loan');
INSERT INTO `tbl_scheme` VALUES ('2', 'Micro Loan');

-- ----------------------------
-- Table structure for `tbl_service`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_service`;
CREATE TABLE `tbl_service` (
  `service_id` int(25) NOT NULL AUTO_INCREMENT,
  `customer_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`service_id`),
  KEY `FK_tbl_service_1` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_service
-- ----------------------------
INSERT INTO `tbl_service` VALUES ('1', '9', '1800', '2030-07-11 04:51:44');
INSERT INTO `tbl_service` VALUES ('2', '10', '850', '2030-07-11 08:39:05');
INSERT INTO `tbl_service` VALUES ('3', '11', '900', '2012-08-11 11:39:41');
INSERT INTO `tbl_service` VALUES ('4', '12', '3500', '2017-08-11 02:09:57');
INSERT INTO `tbl_service` VALUES ('5', '13', '2100', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `tbl_service_charge`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_service_charge`;
CREATE TABLE `tbl_service_charge` (
  `service_charge_id` int(25) NOT NULL AUTO_INCREMENT,
  `charge` double DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`service_charge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_service_charge
-- ----------------------------
INSERT INTO `tbl_service_charge` VALUES ('1', '0.1', '2030-07-11 02:37:10');

-- ----------------------------
-- Table structure for `tbl_states`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_states`;
CREATE TABLE `tbl_states` (
  `StateID` int(10) NOT NULL,
  `StateName` varchar(200) NOT NULL,
  `StateCapital` varchar(50) DEFAULT NULL,
  `StateCode` char(10) DEFAULT NULL,
  `Deleted` varchar(50) DEFAULT NULL,
  `DeletedBy` varchar(30) DEFAULT NULL,
  `CreatedDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_states
-- ----------------------------
INSERT INTO `tbl_states` VALUES ('1', 'Abia', '', 'AB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('2', 'Adamawa', '', 'AD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('3', 'Akwa Ibom', '', 'AK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('4', 'Anambra', '', 'AN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('5', 'Bauchi', '', 'BA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('7', 'Benue', '', 'BN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('8', 'Borno', '', 'BO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('6', 'Bayelsa', '', 'BY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('9', 'Cross Rivers', '', 'CR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('10', 'Delta', '', 'DT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('11', 'Ebonyi', '', 'EB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('12', 'Edo', '', 'ED', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('13', 'Ekiti', '', 'EK', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('14', 'Enugu', '', 'EN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('15', 'FCT', '', 'FC', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('16', 'Gombe', '', 'GM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('17', 'Imo', '', 'IM', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('18', 'Jigawa', '', 'JG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('22', 'Kebbi', '', 'KB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('19', 'Kaduna', '', 'KD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('23', 'Kogi', '', 'KG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('20', 'Kano', '', 'KN', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('21', 'Katsina', '', 'KT', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('24', 'Kwara', '', 'KW', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('25', 'Lagos', '', 'LA', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('27', 'Niger', '', 'NG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('26', 'Nassarawa', '', 'NS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('29', 'Ondo', '', 'OD', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('28', 'Ogun', '', 'OG', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('30', 'Osun', '', 'OS', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('31', 'Oyo', '', 'OY', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('32', 'Plateau', '', 'PL', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('33', 'Rivers', '', 'RV', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('34', 'Sokoto', '', 'SO', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('35', 'Taraba', '', 'TR', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('36', 'Yobe', '', 'YB', '', '', '2008-08-02 18:55:22');
INSERT INTO `tbl_states` VALUES ('37', 'Zamfara', '', 'ZM', '', '', '2008-08-02 18:55:22');

-- ----------------------------
-- Table structure for `tbl_user_type`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_type`;
CREATE TABLE `tbl_user_type` (
  `user_type_id` int(25) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` varchar(50) DEFAULT NULL,
  `user_desc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user_type
-- ----------------------------
INSERT INTO `tbl_user_type` VALUES ('2', 'admin', 'Administrative Priviledge');
INSERT INTO `tbl_user_type` VALUES ('3', 'Supervisor', 'Bank Supervisor');

-- ----------------------------
-- Table structure for `tbl_withdrawal`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_withdrawal`;
CREATE TABLE `tbl_withdrawal` (
  `withdrawal_id` int(25) NOT NULL AUTO_INCREMENT,
  `customer_id` int(25) unsigned DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `acct_officer` varchar(50) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`withdrawal_id`),
  KEY `FK_tbl_withdrawal_1` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_withdrawal
-- ----------------------------
INSERT INTO `tbl_withdrawal` VALUES ('1', '1', '500', 'sysadmin administrator', '2013-09-09 09:23:08');
