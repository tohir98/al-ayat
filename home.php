<?php
ob_start();
session_start();
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: login.php?msg=$msg");
}
include "methods.php";
?>


<!doctype html>
<html>
    <?php include '_header.php'; ?>

    <body>

    </div>

    <div id="navigation">
        <div class="container-fluid">
            <a href="#" id="brand">AL-A</a>
            <a href="#" class="toggle-nav" rel="tooltip" data-placement="bottom" title="Toggle navigation"><i class="icon-reorder"></i></a>
            <ul class='main-nav'>
                <li class=''>
                    <a href="home.php?page=dashboard">
                        <span>Dashboard</span>
                    </a>
                </li>
                <?php
                $query = ExecuteSQLQuery("SELECT id_group, subject FROM tbl_module_group");
                while ($row = mysqli_fetch_array($query)) {
                    ?>
                    <li>
                        <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                            <span><?php echo $row[1]; ?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php
                            $qsub = ExecuteSQLQuery("SELECT subject, id_string FROM tbl_modules WHERE id_group = '" . $row[0] . "' AND status = 1 ");
                            while ($row_sub = mysqli_fetch_array($qsub)) {
                                ?>
                                <li>
                                    <a href="home.php?page=<?php echo $row_sub[1]; ?>"><?php echo $row_sub[0]; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
            <div class="user">
                <ul class="icon-nav">


                    <li class='dropdown colo'>

                        <ul class="dropdown-menu pull-right theme-colors">
                            <li class="subtitle">
                                Predefined colors
                            </li>
                            <li>
                                <span class='red'></span>
                                <span class='orange'></span>
                                <span class='green'></span>
                                <span class="brown"></span>
                                <span class="blue"></span>
                                <span class='lime'></span>
                                <span class="teal"></span>
                                <span class="purple"></span>
                                <span class="pink"></span>
                                <span class="magenta"></span>
                                <span class="grey"></span>
                                <span class="darkblue"></span>
                                <span class="lightred"></span>
                                <span class="lightgrey"></span>
                                <span class="satblue"></span>
                                <span class="satgreen"></span>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="locked.php" class='lock-screen' rel='tooltip' title="Lock screen" data-placement="bottom"><i class="icon-lock"></i></a>
                    </li>
                </ul>
                <div class="dropdown">
                    <a href="#" class='dropdown-toggle' data-toggle="dropdown"><?php echo $_SESSION['fname']; ?> <img src="img/profile-default.jpg" alt="" width="20" height="20"></a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="logout.php?p=staff">Log out</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="content">
        <div id="left">
            <div class="subnav">
                <div class="subnav-title">
                    <span><strong>Al-Ayat Coperative</strong> 
                    </span>
                </div>
            </div>

            <div class="subnav">
                <div class="subnav-title">
                    <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Users</span></a>
                </div>
                <ul class="subnav-menu">
                    <li>
                        <a href="home.php?page=users">Users</a>
                    </li>
                    <li>
                        <a href="home.php?page=users&subpage=user_type">User Type</a>
                    </li>
                    <li>
                        <a href="home.php?page=users&subpage=locations">Locations</a>
                    </li>
                </ul>
            </div>
         

            <div class="subnav">
                <div class="subnav-title">
                    <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Members</span></a>
                </div>
                <ul class="subnav-menu">
                    <li>
                        <a href="home.php?page=accounts">Accounts Directory</a>
                    </li>
                    <li>
                        <a href="home.php?page=accounts&subpage=account_type">Accounts Type</a>
                    </li>
                </ul>
            </div>
            
            <div class="subnav">
                <div class="subnav-title">
                    <a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span>Financial Sheet</span></a>
                </div>
                <ul class="subnav-menu">
                    <li>
                        <a href="home.php?page=financial-sheet">View</a>
                    </li>
                    <li>
                        <a href="home.php?page=financial-sheet&subpage=add">Add Record</a>
                    </li>
                </ul>
            </div>




        </div>
        <div id="main" style="padding:5px 5px">

            <?php
            $page = isset($_GET["page"]) ? $_GET["page"] : '';
            if ($page == "users")
                include("users/index.php");
            elseif ($page == "user_type")
                include("users/user_type.php");
            elseif ($page == "members")
                include("members/index.php");
            elseif ($page == "accounts")
                include("accounts/index.php");

            elseif ($page == "transactions")
                include("transactions/index.php");

            elseif ($page == "other-income")
                include("other-income/index.php");

            elseif ($page == "financial-sheet")
                include("financial-sheet/index.php");

            elseif ($page == "receipt-payment")
                include("receipt-payment/index.php");

            elseif ($page == "dl")
                include("dev-levy/index.php");

            elseif ($page == "shares")
                include("shares/index.php");

            elseif ($page == "savings")
                include("savings/index.php");

            elseif ($page == "insurance")
                include("insurance/index.php");
            elseif ($page == "ileya")
                include("ileya/index.php");
            elseif ($page == "sec")
                include("sec/index.php");
            elseif ($page == "ots")
                include("ots/index.php");
            elseif ($page == "loan")
                include("loan/index.php");
            elseif ($page == "lrp")
                include("lrp/index.php");
            elseif ($page == "sds")
                include("sds/index.php");
            elseif ($page == "sdsr")
                include("sdsr/index.php");
            elseif ($page == "hajj")
                include("hajj/index.php");
            else
                include("dashboard.php");
            ?>


        </div>
    </div>

</body>

</html>