<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php?page=transactions">Transactions</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Deposit</a>
			<i class="icon-angle-right"></i>
        </li>
    </ul>
</div>
<!-- display errors here -->

<div class="row-fluid">
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-ok"></i>
									Deposit
								</h3>
							</div>
							<div class="box-content">
								<form action="home.php?page=accounts&subpage=process_account&action=add_account" method="POST" class='form-horizontal form-validate' id="frmnew_account">
									<div class="control-group">
										<label for="textfield" class="control-label">Account Number *</label>
										<div class="controls">
											<input type="text" name="account_num" id="account_num" class="input-large" data-rule-required="true" data-rule-minlength="10">
										</div>
									</div>
									
									
									<div class="form-actions">
										<input type="submit" class="btn btn-primary" value="Submit">
										<button type="button" class="btn">Cancel</button>
									</div>
								</form>
							</div>
						</div>
					</div>
<script>
	
var countries_url = '<?php echo site_url('recruiting/recruiting/states_select/'); ?>'; 
$(document).ready(function() {
	
	<?php if($cur_country > 0) { ?>
	load_states(<?php echo $cur_country; ?>, <?php echo $cur_state; ?>);
	<?php } ?>
	
	$('#id_country').change(function() {
		var id_country = $(this).val();
		load_states(id_country, 0);
	});
	
	function load_states(id_country, id_state) {
				
		var ajax_url = countries_url + '/' + id_country + '/single/id_state/'+id_state;
		
		$.ajax({
		  type: 'POST',
		  url: ajax_url,
		  cache: false,
		  dataType: "html",
		  
		  beforeSend: function(html) {
			$('#states_container').html('Loading states...'); 
		  },
		  		  
	      success: function(html){
			$('#states_container').html(html); 
		  },	
		  error: function(html) {
			$('#states_container').html('Error: cannot load data.'); 
		  }
		 
		});
		
	}
		
});	
</script>