<?php
	
	if($_GET["action"] == "add_account"){
	
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$gender_id = $_POST['gender_id'];
		$marital_status_id = $_POST['marital_status_id'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state_id = $_POST['state_id'];
		$local_govt_id = $_POST['local_govt_id'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$acct_type_id = $_POST['acct_type_id'];
		$open_date = date('Y-m-d h:i:s');
		$creator_id = $_SESSION['role_id'];
		$location_id = $_SESSION['location_id'];
		
		// -----------------to generate acct no -----------------
		$acct_no = getLastId('tbl_customer', 'customer_id');
		$acct_num="211001".str_pad($acct_no,4,"0",STR_PAD_LEFT);
	//------------------------------------------------------
		
		
		if (!check('tbl_customer','phone',$phone)) {
			$q = mysql_query("INSERT INTO tbl_customer (first_name
			, Last_name
			, gender_id
			, marital_status_id
			, address
			, city
			, regional_state_id
			, local_govt_id
			, phone
			, email
			, acct_type_id, open_date, acct_no, creator_id, location_id) VALUES('$first_name'
			, '$last_name'
			, '$gender_id'
			, '$marital_status_id'
			, '$address'
			, '$city'
			, '$state_id'
			, '$local_govt_id'
			, '$phone'
			, '$email'
			, '$acct_type_id', '$open_date', '$acct_num', '$creator_id', '$location_id')");
			if ($q)
				$_SESSION['success'] = "User account created successfully";
			else
				$_SESSION['error'] = mysql_error();//"Error creating user account";
		}else{
			$_SESSION['error'] = "User account already exist";
		}//End if check
		
		header('location:home.php?page=accounts');
	}//End if action=add_account
	
	
	//add_acct_type
	else if($_GET["action"] == "add_acct_type"){
	
		$acct_type = $_POST['acct_type'];
		$description = $_POST['description'];
		$charge = $_POST['charge']/100;
		$datetime = date('Y-m-d h:i:s');
		$creator_id = $_SESSION['role_id'];
		
		if (!check('tbl_acct_type','acct_type',$acct_type)) {
			$q = mysql_query("INSERT INTO tbl_acct_type (acct_type, description, charge, datetime, added_by) VALUES('$acct_type', '$description', '$charge', '$datetime', '$creator_id')");
			if ($q)
				$_SESSION['success'] = "Account type created successfully";
			else
				$_SESSION['error'] = "Error creating account type";
		}else{
			$_SESSION['error'] = "Account type already exist";
		}//End if check
		
		header('location:home.php?page=accounts&subpage=account_type');
	}//End if action = add_acct_type
	
		//add_location
	else if($_GET["action"] == "add_location"){
	
		$location_tag = $_POST['location_tag'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state_id = $_POST['state_id'];
		$parent_id = $_POST['parent_id'];
		$head_id = $_POST['head_id'];
		$date_created = date('Y-m-d h:i:s');
		$creator_id = $_SESSION['role_id'];
		
		if (!check('tbl_locations','location_tag',$location_tag)) {
			$q = mysql_query("INSERT INTO tbl_locations (location_tag, address, city, state_id, parent_id, head_id, date_created, creator_id) VALUES('$location_tag', '$address', '$city', '$state_id', '$parent_id', '$head_id', '$date_created', '$creator_id')");
			if ($q)
				$_SESSION['success'] = "Location added successfully";
			else
				$_SESSION['error'] = "Error adding location";
		}else{
			$_SESSION['error'] = "Location already exist";
		}//End if check
		
		header('location:home.php?page=users&subpage=locations');
	}//End if action = add_location
	
	
	//edit_user_type
	else if($_GET["action"] == "edit_user_type"){
	
		$user_type = $_POST['user_type'];
		$user_desc = $_POST['user_desc'];
		$user_type_id = $_POST['user_type_id'];
		
		$q = mysql_query("UPDATE tbl_user_type SET  user_type = '$user_type', user_desc = '$user_desc' WHERE user_type_id = '$user_type_id' ");
			if ($q)
				$_SESSION['success'] = "User type updated successfully";
			else
				$_SESSION['error'] = "Error updating user type";
		
		header('location:home.php?page=users&subpage=user_type');
	}//End if action = edit_user_type
	
	
	//edit_user
	else if($_GET["action"] == "edit_user"){
	
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$user_type_id = $_POST['user_type_id'];
		$role_id = $_POST['role_id'];
		
		$q = mysql_query("UPDATE tbl_role SET  Firstname = '$first_name', Lastname = '$last_name', email = '$email', user_type_id = '$user_type_id'  WHERE role_id = '$role_id' ");
			if ($q)
				$_SESSION['success'] = "User updated successfully";
			else
				$_SESSION['error'] = "Error updating user info";
		
		header('location:home.php?page=users');
	}//End if action = edit_user
	
	
	else if ($_GET["action"] == "delete_acct_type"){
	
		$id = $_GET['id'];
	
		$where = 'acct_type_id ='. $id;
		$q = delete('tbl_acct_type',$where);
		
		if($q){
			$_SESSION['success'] = "Account type deleted successfully";
		}else{
			$_SESSION['error'] = "Error while deleting user type"; 
		}
		
		header('location:home.php?page=accounts&subpage=account_type');
		
	}//End if action is delete_acct_type
	
	else if ($_GET["action"] == "delete"){
		$id = $_GET['id'];
	
		//$where = 'customer_id ='. $id;
		//$q = delete('tbl_customer',$where);
		$q = mysql_query("UPDATE tbl_customer SET  deleted = 'yes'  WHERE customer_id = '$id' ");
		
		if($q){
			$_SESSION['success'] = "Account deleted successfully";
		}else{
			$_SESSION['error'] = "Error while deleting user type"; 
		}
		
		header('location:home.php?page=accounts');
		
	}//End if action is delete_account
	
		else if ($_GET["action"] == "activate"){
			
			$pin = $_POST['pin1'];
			if ( !empty($pin)) {
				foreach ( $pin as $p) {
					$q = mysql_query("UPDATE tbl_customer SET  deleted = 'no'  WHERE customer_id = '$p' ");
				}
			}
			
			if($q){
				$_SESSION['success'] = "Account(s) activated successfully";
			}else{
				$_SESSION['error'] = "Error while activating accounts"; 
			}
			
			header('location:home.php?page=accounts');
		
	}//End if action is activate
	
			else if ($_GET["action"] == "deactivate"){
			
			$pin = $_POST['pin'];
			if ( !empty($pin)) {
				foreach ( $pin as $p) {
					$q = mysql_query("UPDATE tbl_customer SET  deleted = 'yes'  WHERE customer_id = '$p' ");
				}
			}
			
			if($q){
				$_SESSION['success'] = "Account(s) deactivated successfully";
			}else{
				$_SESSION['error'] = "Error while deactivating accounts"; 
			}
			
			header('location:home.php?page=accounts');
		
	}//End if action is deactivate

	
	
	
	
?>