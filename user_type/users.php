<!-- Display error/success message -->
<br/>
<?php if ($_SESSION['error'] != '') { ?>
<div class="alert alert-danger">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $_SESSION['error'] ; ?>
</div>
<?php $_SESSION['error'] = '' ; } 

if ($_SESSION['success'] != '') { ?>
<div class="alert alert-success">
     <button data-dismiss="alert" class="close" type="button">x</button>
     <?php echo $_SESSION['success']; ?>
</div>
<?php $_SESSION['success'] = ''; }  ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Users</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        
    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="">Users</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a>Users</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <h3>
			<button href="#create_new_user_modal" data-toggle="modal" class="btn btn-warning">Create New User</button>
        </h3>
       
    </div>
    <div class="box-content nopadding">
        <div class="tab-content">
           
            
            <div class="tab-pane active" id="enrolled">
                
                
                <table class="table table-user table-hover table-nomargin dataTable">
                                <thead>
                                        <tr>
                                                <th>SN</th>
                                                <th>Name</th>
                                                <th class='hidden-1024'>Email</th>
                                                <th class='hidden-480'>User Type</th>
                                                <th class='hidden-480'>Location</th>
                                                <th class='hidden-480' nowrap>Action</th>
                                        </tr>
                                </thead>
                                <tbody>
                                <?php 
									$Rqry = mysql_query("SELECT r.* , ut.user_type
												FROM tbl_role r
												left JOIN tbl_user_type ut ON r.user_type_id = ut.user_type_id");
									  $counter = 1;
									  while ($rowRqry = mysql_fetch_array($Rqry)){  ?>
										<tr>
											<td> <?php echo $counter ?>  </td>
											<td> <?php echo $rowRqry["Firstname"]; ?> <?php echo $rowRqry["Lastname"]; ?> </td>
											<td> <?php echo $rowRqry["email"]; ?> </td>
											<td>
													<span class="muted"><?php echo $rowRqry["user_type"]; ?></span>
											</td>
											<td><?php echo "Location"; ?></td>
											<td>
													<div class="btn-group">
															<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
															<ul class="dropdown-menu">
																	<li><a href="home.php?page=users&subpage=view_user&id=<?php echo $rowRqry["role_id"]?>">View Records </a></li>
																	<li><a href="home.php?page=users&subpage=edit_user&id=<?php echo $rowRqry["role_id"]?>">Edit User </a></li>
																	<li><a href="home.php?page=users&subpage=delete_user&id=<?php echo $rowRqry["role_id"]?>" class="modal_confirm">Delete User </a></li>
																	
															</ul>
													</div>
											</td>
									</tr>
                                <?php $counter++; } ?>

                            </tbody>
                        </table>
                
                
            </div><!-- End Div Enrolled -->
			 

        </div>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-body">
    <p>Are you sure you want to delete user?</p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
		<a href="#" class="btn btn-danger closeme">Yes, delete user</a>
  </div>
</div>
<!-- End Remove Shift Modal -->


<!-- Add User Modal -->
<div class="modal hide fade" id="create_new_user_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    
<div class="row_fluid"> 
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 class="modal-title" id="myModalLabel">Create User</h4>
    </div>
    
                <form name="add_user" id="add_user" method="post" action="home.php?page=users&subpage=process_user&action=add" class="form-horizontal">

                        <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">First Name</label>
                            <div class="controls">
                                <input type="text" name="first_name" id="first_name" class="input text">
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">LastName</label>
                            <div class="controls">
                                <input type="text" name="last_name" id="last_name" class="input text">
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Email</label>
                            <div class="controls">
                                <input type="text" name="Email" id="Email" class="input text">
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">User Type</label>
                            <div class="controls">
							<?php
								$Qemp2 = "SELECT * FROM tbl_user_type";
								$Remp2 = mysql_query($Qemp2);
							?>
							<select name="user_type_id" id="user_type_id" class="select2-me input-xlarge">
								<option value="0">Select user type</option>
								<?php while($Rowsemp2=mysql_fetch_array($Remp2)){ ?>
								<option value="<?php echo $Rowsemp2['user_type_id']; ?>"><?php echo $Rowsemp2['user_type'];?></option>
								<?php } ?>
						  </select>
                            </div>
                        </div>


						<div class="form-actions">
							<button type="submit" class="btn btn-primary">Create User</button>
						</div>
					
					
				</form>
                </div>
</div>

<script type="text/javascript">
    
    $('.modal_confirm').click(function(eve){
        
        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();
        
        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
        
    });
    
    
    
    $('.modal_confirm_shift').click(function(eve){
        
        eve.preventDefault();
        $('#modal-3').modal('show').fadeIn();
        
        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);
        
    });
    
    
    
    $('.ajaxlink').click(function(eve){
        
        eve.preventDefault();
        $('#contribution').modal('show');
        $('#contribution').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#contribution').html('');
          $('#contribution').html(html).show();

          
        });
        
    });
    
    
    $('.ajaxlink_shift').click(function(eve){
        
        eve.preventDefault();
        $('#edit_shift_dialog').modal('show');
        $('#edit_shift_dialog').html('<div class="loaderBox"><img src="/img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#edit_shift_dialog').html('');
          $('#edit_shift_dialog').html(html).show();

          
        });
        
    });

    
</script>