<?php
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: ../login.php?msg=$msg");
}
?>
<?php include './_notification.php'; ?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <a class="btn btn-warning" href="home.php?page=dashboard"><i class="icon icon-arrow-left"></i>&nbsp;Back to dashboard</a>
        <h1>HAJJ</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="../insurrance/home.php">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">HAJJ</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <h3>
            -
        </h3>

    </div>
    <div class="box-content nopadding">
        <div class="tab-content">


            <div class="tab-pane active" id="enrolled">


                <table class="table table-user table-hover table-nomargin dataTable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th class='hidden-1024'>Total</th>
                            <th class='hidden-480'>Month</th>
                            <th class='hidden-480'>Year</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $Rqry = ExecuteSQLQuery("SELECT fs.hajj,fs.month, fs.year, c.first_name, c.last_name
												FROM tbl_financial_sheet fs
												INNER JOIN tbl_customer c ON fs.customer_id = c.customer_id");
                        $sn = 0;
                        $sum = 0;
                        while ($rowRqry = mysqli_fetch_array($Rqry)) {
                            ?>
                            <tr>
                                <td> <?= ++$sn ?>  </td>
                                <td> <?= ucfirst($rowRqry["first_name"]) . ' ' . ucfirst($rowRqry["last_name"]); ?></td>
                                <td> <?php echo number_format($rowRqry[0], 2); ?> </td>
                                <td><?php echo $months[$rowRqry["month"]]; ?></td>
                                <td><?php echo $rowRqry["year"]; ?></td>

                            </tr>
                            <?php
                            $sum += $rowRqry[0];
                        }
                        ?>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>&nbsp; </th>
                            <th> Total:</th>
                            <th><?php echo number_format($sum, 2); ?></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>


            </div><!-- End Div Enrolled -->


        </div>
    </div>    
</div>

