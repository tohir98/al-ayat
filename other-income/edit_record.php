<?php
session_start();
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: ../login.php?msg=$msg");
}

include("../methods.php");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $Qchker = "SELECT oi.*, c.first_name, c.last_name FROM 
					tbl_other_income oi, tbl_customer c
					WHERE oi.customer_id = c.customer_id
					AND oi.other_income_id =  '$id'";

    $Rchker = ExecuteSQLQuery($Qchker);

    if (mysqli_num_rows($Rchker) > 0) {
        $RowRchker = mysqli_fetch_array($Rchker);

        $other_income_id = $RowRchker['other_income_id'];
        $first_name = $RowRchker['first_name'];
        $last_name = $RowRchker['last_name'];
        $customer_id = $RowRchker['customer_id'];
        $reg_form = $RowRchker['reg_form'];
        $loan_form = $RowRchker['loan_form'];
        $donation = $RowRchker['donation'];
        $lateness_fine = $RowRchker['lateness_fine'];
        $agm_fee = $RowRchker['agm_fee'];
        $cot = $RowRchker['cot'];
        $zakak_sadaqah = $RowRchker['zakak_sadaqah'];
        $id_card = $RowRchker['id_card'];
        $nec_dues = $RowRchker['nec_dues'];
        $file = $RowRchker['file'];
        $other_income = $RowRchker['other_income'];
        $month = $RowRchker['month'];
        $year = $RowRchker['year'];
        $edit = "1";
    }
}
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4 id="myModalLabel">Other Incomes [<?php echo $first_name . ' ' . $last_name; ?>]&nbsp;&nbsp;&nbsp;<div class="label"> <?php echo $months[$month]; ?></div>
        <div class="label"><?php echo $year ?></div> </h4>
</div>


<form class="form-horizontal form-bordered" id="frm_financial_sheet" method="post" action="home.php?page=other-income&subpage=process-income&action=edit">
    <div class="modal-body nopadding">

        <table class="table table-striped">

            <div class="control-group">
                <label for="select" class="control-label">Reg. Form</label>
                <div class="controls">
                    <input type="text" id="reg_form" name="reg_form" value="<?php if (isset($reg_form)) {
    echo $reg_form;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Loan Form</label>
                <div class="controls">
                    <input type="text" id="loan_form" name="loan_form" value="<?php if (isset($loan_form)) {
    echo $loan_form;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Donation</label>
                <div class="controls">
                    <input type="text" id="donation" name="donation" value="<?php if (isset($donation)) {
    echo $donation;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Lateness Fine</label>
                <div class="controls">
                    <input type="text" id="lateness_fine" name="lateness_fine" value="<?php if (isset($lateness_fine)) {
    echo $lateness_fine;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">AGM Fee</label>
                <div class="controls">
                    <input type="text" id="agm_fee" name="agm_fee" value="<?php if (isset($agm_fee)) {
    echo $agm_fee;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">COT</label>
                <div class="controls">
                    <input type="text" id="cot" name="cot" value="<?php if (isset($cot)) {
    echo $cot;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Zakak/Sadaqah</label>
                <div class="controls">
                    <input type="text" id="zakak_sadaqah" name="zakak_sadaqah" value="<?php if (isset($zakak_sadaqah)) {
    echo $zakak_sadaqah;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">ID Card</label>
                <div class="controls">
                    <input type="text" id="id_card" name="id_card" value="<?php if (isset($id_card)) {
    echo $id_card;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">NEC Dues</label>
                <div class="controls">
                    <input type="text" id="nec_dues" name="nec_dues" value="<?php if (isset($nec_dues)) {
    echo $nec_dues;
} ?>" />
                </div>
            </div>

            <div class="control-group" >
                <label for="select" class="control-label">File</label>
                <div class="controls">
                    <input type="text" id="file" name="file" value="<?php if (isset($file)) {
    echo $file;
} ?>" />
                </div>
            </div>

            <div class="control-group">
                <label for="select" class="control-label">Other Income</label>
                <div class="controls">
                    <input type="text" id="other_income" name="other_income" value="<?php if (isset($other_income)) {
    echo $other_income;
} ?>" />
                </div>
            </div>


        </table>

        <div class="modal-footer">
            <input type="hidden" id="other_income_id" name="other_income_id" value="<?php echo $other_income_id ?>" />
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button type="button" name="btn_update" id="btn_update" class="btn btn-warning pull-right">Update</button>
        </div>
</form>



<script>

    $("#btn_update").click(function () {
        $("#btn_update").prop('disabled', true);
        $("#frm_financial_sheet").submit();
    });

    $('input[type=submit]').click(function (e) {
        $(this).prop('disabled', true);
        $('form').submit();
    });

</script>