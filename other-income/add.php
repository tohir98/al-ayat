<div class="row-fluid">
	<div class="page-header">
		<div class="pull-left">
			<a href="home.php?page=other-income" class="btn btn-warning"><i class="icon-arrow-left"></i> &nbsp;Back</a>
			<br />
			<h1>New Record</h1>
		</div>
		<div class="clearfix"></div>
	</div>
	
	<!-- Breadcrumb -->
	<div class="breadcrumbs">
		<ul>
			<li>
				<a href="home.php">Dashboard</a>
				<i class="icon-angle-right"></i>
			</li>
			<li>
				<a href="home.php?page=other-income">Other Incomes</a>
				<i class="icon-angle-right"></i>
			</li>
			<li>
				<a href="#">Add Record</a>
				<i class="icon-angle-right"></i>
			</li>
		</ul>
	</div>
	<!-- End Breadcrumb -->
	
<!-- display errors here -->

	<div class="row-fluid">
		<div class="span12">
			<div class="box box-bordered">
				<div class="box-title">
					<h3>
						<i class="icon-ok"></i>
						Add Other Income
					</h3>
				</div>
				<div class="box-content">
					<form id="frmadd" action="home.php?page=other-income&subpage=process_income&action=add" method="POST" class='form-horizontal form-validate form-striped form-condensed'>
						<div class="control-group">
							<label for="textfield" class="control-label">Member</label>
							<div class="controls">
								<?php $Rusr = ExecuteSQLQuery("SELECT * FROM tbl_customer WHERE location_id = '".$_SESSION['location_id']."' ");?>
								<select name="customer_id" id="customer_id" class="select2-me input-large">
									<option value="0">Select member</option>
									<?php while($Rowsusr=mysqli_fetch_array($Rusr)){ ?>
									<option value="<?php echo $Rowsusr['customer_id']; ?>"><?php echo ucfirst(trim($Rowsusr[1]))." ".ucfirst(trim($Rowsusr[2]));?></option>
									<?php } ?>
								</select>
                            </div>
						</div>
						<div class="control-group">
							<label for="textfield" class="control-label">Reg Form</label>
							<div class="controls">
								<input type="text" name="reg_form" id="reg_form" class="compute" value="0">
							</div>
						</div>
						<div class="control-group">
							<label for="emailfield" class="control-label">Loan Form</label>
							<div class="controls">
								<input type="text" name="loan_form" id="loan_form" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="pwfield" class="control-label">Donation</label>
							<div class="controls">
								<input type="text" name="donation" id="donation" class="compute"  value="0"/>
							</div>
						</div>
						<div class="control-group">
							<label for="confirmfield" class="control-label">Lateness/Fine</label>
							<div class="controls">
								<input type="text" name="lateness_fine" id="lateness_fine" class="compute" value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="policy" class="control-label">AGM Fees</label>
							<div class="controls">
								<input type="text" name="agm_fee" id="agm_fee" class="compute"  value="0"/>
							</div>
						</div>
						<div class="control-group">
							<label for="COT" class="control-label">COT</label>
							<div class="controls">
								<input type="text" name="cot" id="cot" class="compute"  value="0"/>
							</div>
						</div>
						<div class="control-group">
							<label for="Zakat" class="control-label">Zakat/Sadakat</label>
							<div class="controls">
								<input type="text" id="zakak_sadaqah" name="zakak_sadaqah" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">ID Card</label>
							<div class="controls">
								<input type="text" id="id_card" name="id_card" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">NEC Dues</label>
							<div class="controls">
								<input type="text" id="nec_dues" name="nec_dues" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">File</label>
							<div class="controls">
								<input type="text" id="file" name="file" class="compute"  value="0" />
							</div>
						</div>
						<div class="control-group">
							<label for="id card" class="control-label">Other Income</label>
							<div class="controls">
								<input type="text" id="other_income" name="other_income" class="compute"  value="0" />
							</div>
						</div>
						
						<div class="control-group">
							<label for="id card" class="control-label">Total</label>
							<div class="controls">
								<div id="total" name="total" class="" ></div>
							</div>
						</div>
						
						<div class="form-actions">
							<input type="button" id="btn_save" class="btn btn-primary" value="Save">
							<button type="button" class="btn">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	
<script>
	$(document).ready(function() {
    
	//	Action for Save button
	$('#btn_save').click(function() {
            //show_loader();
            $('#btn_save').attr('disabled', true);
			$('#frmadd').submit();
			return false;
		
	});	
	
	//	Change function
	$('.compute').keyup(function() {
	
			 var total = parseInt($('#reg_form').val()) 
			 			+ parseInt($('#loan_form').val()) 
						+ parseInt($('#donation').val()) 
						+ parseInt($('#lateness_fine').val()) 
						+ parseInt($('#agm_fee').val()) 
						+ parseInt($('#cot').val()) 
						+ parseInt($('#zakak_sadaqah').val()) 
						+ parseInt($('#id_card').val()) 
						+ parseInt($('#nec_dues').val()) 
						+ parseInt($('#file').val()) 
						+ parseInt($('#other_income').val())  ;
						
            $('#total').html('<b>'+total+'</b>');
			return false;
		
	});	
	//	End change function
	
});
</script>
		
		