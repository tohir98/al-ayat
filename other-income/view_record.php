<?php
	session_start();
	include("../methods.php");
	if (isset($_GET['id'])){
	$id = $_GET['id'];
	$Qchker = "SELECT fs.* , fs.reg_form + fs.loan_form + fs.donation + fs.lateness_fine + fs.agm_fee + fs.cot + fs.zakak_sadaqah + fs.id_card + fs.nec_dues + fs.other_income as total, c.first_name, c.last_name
				FROM tbl_other_income fs
				INNER JOIN tbl_customer c ON fs.customer_id = c.customer_id WHERE fs.other_income_id = '$id'";
	$Rchker = ExecuteSQLQuery($Qchker);
	if (mysqli_num_rows($Rchker) > 0){
	$RowRchker = mysqli_fetch_array($Rchker);
	
	$other_income_id   = $RowRchker['other_income_id'];
	$first_name = $RowRchker['first_name'];
	$last_name = $RowRchker['last_name'];
	$reg_form = $RowRchker['reg_form'];
	$loan_form = $RowRchker['loan_form'];
	$donation = $RowRchker['donation'];
	$lateness_fine = $RowRchker['lateness_fine'];
	$agm_fee = $RowRchker['agm_fee'];
	$cot = $RowRchker['cot'];
	$zakak_sadaqah = $RowRchker['zakak_sadaqah'];
	$id_card = $RowRchker['id_card'];
	$nec_dues = $RowRchker['nec_dues'];
	$file = $RowRchker['file'];
	$other_income = $RowRchker['other_income'];
	
	$total = $RowRchker['total'];
	
	$month = $RowRchker['month'];
	$year = $RowRchker['year'];
	$edit = "1";
	}}
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h4 id="myModalLabel"> <?php echo ucfirst($first_name) .' '. ucfirst($last_name); ?>&nbsp; [NGN &nbsp;<?php echo number_format($total,2); ?>] &nbsp;<div class="label"> <?php echo $months[$month]; ?></div>
	<div class="label"><?php echo $year ?></div> </h4>
</div>
<div class="modal-body padless">

	<form class="form-horizontal form-striped" id="frm_edit_user_type" method="post" action="home.php?page=users&subpage=process_user&action=edit_user">
	<table class="table table-striped table-condensed">
	
		<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Reg. Form</label>
                            <div class="controls">
							<label for="select" class="control-label"><?php if (isset($reg_form)){ echo number_format($reg_form,2); }else{ echo "0.00"; } ?></label>
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Loan Form</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($loan_form) and is_numeric($loan_form) ){ echo number_format($loan_form,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>
						
						<div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Donation</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($donation)  and is_numeric($donation) ){ echo number_format($donation,2);} else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Lateness Fine</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($lateness_fine)  and is_numeric($lateness_fine) ){ echo number_format($lateness_fine,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">AGM Fee</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($agm_fee)  and is_numeric($agm_fee) ){ echo number_format($agm_fee,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">COT</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($cot)  and is_numeric($cot) ){ echo number_format($cot,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Zakat/Zadakat</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($zakak_sadaqah)  and is_numeric($zakak_sadaqah) ){ echo number_format($zakak_sadaqah,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">ID Card</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($id_card) and is_numeric($id_card) ){ echo number_format($id_card,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">NEC Dues</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($nec_dues)  and is_numeric($nec_dues) ){ echo number_format($nec_dues,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">File</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($file) and is_numeric($file) ){ echo number_format($file,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

                       <div class="control-group" style="margin-left: 30px">
                            <label for="select" class="control-label">Other Income</label>
                            <div class="controls">
								<label for="select" class="control-label"><?php if (isset($other_income) and is_numeric($other_income) ){ echo number_format($other_income,2);}else{ echo "0.00"; } ?></label>
                            </div>
                        </div>

		
	</table>
	<form>
</div>

<div class="modal-footer">
	
	<a href="other-income/edit_record.php?id=<?php echo $other_income_id ?>" class="btn btn-success ajaxlink_user" data-dismiss="modal">Edit </a>
	<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
</div>

<!-- Edit dialog -->
<div id="edit_user_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>



<script>
	
	$("#btn_edit_user").click(function() {
		$("#frm_edit_user_type").submit();
		$("#btn_edit_user_type").prop('disabled', true);
    });  

    $('input[type=submit]').click(function(e){
		$('form').submit();
	    $(this).prop('disabled', true);
	});
	
	    $('.ajaxlink_user').click(function(eve){
        
      	eve.preventDefault();
        $('#edit_user_modal').modal('show');
        $('#edit_user_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function(html){
          
          $('#edit_user_modal').html('');
          $('#edit_user_modal').html(html).show();

          
        });
        
    });

</script>