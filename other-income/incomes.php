<?php
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: ../login.php?msg=$msg");
}

?>
<!-- Display error/success message -->
<br/>
<?php if ($_SESSION['error'] != '') { ?>
    <div class="alert alert-danger">
        <button data-dismiss="alert" class="close" type="button">x</button>
        <?php echo $_SESSION['error']; ?>
    </div>
    <?php
    $_SESSION['error'] = '';
}

if ($_SESSION['success'] != '') {
    ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">x</button>
    <?php echo $_SESSION['success']; ?>
    </div>
    <?php $_SESSION['success'] = '';
}
?>

<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h1>Other Income</h1>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">

    </div>
</div>

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Other Income</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <h3>
            <a href="home.php?page=other-income&subpage=add" class="btn btn-warning">Add Record</a>
        </h3>

    </div>
    <div class="box-content nopadding">
        <div class="tab-content">


            <div class="tab-pane active" id="enrolled">


                <table class="table table-user table-hover table-nomargin dataTable">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Name</th>
                            <th class='hidden-1024'>Reg Form</th>
                            <th class='hidden-480'>Loan Form</th>
                            <th class='hidden-480'>Donation</th>
                            <th class='hidden-480'>Lateness/Fine</th>
                            <th class='hidden-480'>Total</th>
                            <th class='hidden-480'>Month</th>
                            <th class='hidden-480'>Year</th>
                            <th class='hidden-480' nowrap>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $Rqry = ExecuteSQLQuery("SELECT oi.* , oi.reg_form+oi.loan_form+oi.donation+oi.lateness_fine+oi.agm_fee+oi.cot+oi.zakak_sadaqah+oi.id_card+oi.nec_dues+oi.file+oi.other_income as total, c.first_name, c.last_name
												FROM tbl_other_income oi
												INNER JOIN tbl_customer c ON oi.customer_id = c.customer_id");
                        $counter = 1;
                        while ($rowRqry = mysqli_fetch_array($Rqry)) {
                            ?>
                            <tr>
                                <td><?php echo $counter ?>  </td>
                                <td><?= ucfirst($rowRqry["first_name"]) .' '. ucfirst($rowRqry["last_name"]); ?> </td>
                                <td><?php echo number_format($rowRqry["reg_form"], 2); ?> </td>
                                <td><?php echo number_format($rowRqry["loan_form"], 2); ?></td>
                                <td><?php echo number_format($rowRqry["donation"], 2); ?></td>
                                <td><?php echo number_format($rowRqry["lateness_fine"], 2); ?></td>
                                <td><?php echo number_format($rowRqry["total"], 2); ?></td>
                                <td><?php echo $months[$rowRqry["month"]]; ?></td>
                                <td><?php echo $rowRqry["year"]; ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="other-income/view_record.php?id=<?php echo $rowRqry["other_income_id"] ?>" class="ajaxlink_view">View </a></li>
                                            <li><a href="other-income/edit_record.php?id=<?php echo $rowRqry["other_income_id"] ?>" class="ajaxlink_edit">Edit </a></li>
                                            <li><a href="home.php?page=other-income&subpage=process-income&action=delete_record&id=<?php echo $rowRqry["other_income_id"] ?>" class="modal_confirm">Delete </a></li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
    <?php $counter++;
}
?>

                    </tbody>
                </table>


            </div><!-- End Div Enrolled -->


        </div>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-body">
        <p>Are you sure you want to delete this record?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, delete</a>
    </div>
</div>
<!-- End Remove Shift Modal -->


<!-- Edit user dialog -->
<div id="edit_record_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<!-- View user dialog -->
<div id="view_record_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script type="text/javascript">

    $('.modal_confirm').click(function (eve) {

        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });



    $('.modal_confirm_shift').click(function (eve) {

        eve.preventDefault();
        $('#modal-3').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });


    $('.ajaxlink_view').click(function (eve) {

        eve.preventDefault();
        $('#view_record_modal').modal('show');
        $('#view_record_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#view_record_modal').html('');
            $('#view_record_modal').html(html).show();


        });

    });


    $('.ajaxlink_edit').click(function (eve) {

        eve.preventDefault();
        $('#edit_record_modal').modal('show');
        $('#edit_record_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#edit_record_modal').html('');
            $('#edit_record_modal').html(html).show();


        });

    });


</script>