<div class="container-fluid">
				<!-- dashboard metro icons -->
				<div class="row-fluid">
					<div class="span12"><ul class="tiles"><li class="brown dashboard_icon">
								<a href="home.php?page=users">
									<span>
										<i class="icon-user"></i>
									</span>
									<span class="name" style="text-align:center">
										Users
									</span>
								</a>
							</li><li class="blue dashboard_icon">
								<a href="home.php?page=accounts">
									<span>
										<i class="icon-book"></i>
									</span>
									<span class="name" style="text-align:center">
										Accounts
									</span>
								</a>
							</li>
							<li class="satblue dashboard_icon">
								<a href="home.php?page=financial-sheet">
									<span>
										<i class="icon-inbox"></i>
									</span>
									<span class="name" style="text-align:center">
										Financial Sheet
									</span>
								</a>
							</li>
							<li class="teal dashboard_icon">
								<a href="home.php?page=other-income">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Other Income
									</span>
								</a>
							</li>
							<li class="red dashboard_icon">
								<a href="home.php?page=receipt-payment">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Receipt & payment
									</span>
								</a>
							</li>
							
							<li class="teal dashboard_icon">
								<a href="home.php?page=dl">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Development Levy
									</span>
								</a>
							</li>
							<li class="pink dashboard_icon">
								<a href="home.php?page=shares">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Shares
									</span>
								</a>
							</li>
							<li class="purple dashboard_icon">
								<a href="home.php?page=savings">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Savings
									</span>
								</a>
							</li>
							<li class="grey dashboard_icon">
								<a href="home.php?page=insurance">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Insurance
									</span>
								</a>
							</li>
							<li class="pink dashboard_icon">
								<a href="home.php?page=ileya">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Ileya
									</span>
								</a>
							</li>
							<li class="lime dashboard_icon">
								<a href="home.php?page=sec">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										SEC
									</span>
								</a>
							</li>
							<li class="magenta dashboard_icon">
								<a href="home.php?page=ots">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										OTS
									</span>
								</a>
							</li>
							<li class="green dashboard_icon">
								<a href="home.php?page=loan">
									<span>
										<i class="icon-credit-card"></i>
									</span>
									<span class="name" style="text-align:center">
										Loan Released
									</span>
								</a>
							</li>
							<li class="green dashboard_icon">
								<a href="home.php?page=lrp">
									<span>
										<i class="icon-credit-card"></i>
									</span>
									<span class="name" style="text-align:center">
										Loan Repayment
									</span>
								</a>
							</li>
							<li class="orange dashboard_icon">
								<a href="home.php?page=sds">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										SDS Released
									</span>
								</a>
							</li>
							<li class="darkblue dashboard_icon">
								<a href="home.php?page=sdsr">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										SDS Repayment
									</span>
								</a>
							</li>
							<li class="satblue dashboard_icon">
								<a href="home.php?page=hajj">
									<span>
										<i class="icon-plane"></i>
									</span>
									<span class="name" style="text-align:center">
										HAJJ
									</span>
								</a>
							</li>
							<li class="lightred dashboard_icon">
								<a href="home.php?page=reports">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Financial Summary
									</span>
								</a>
							</li>
							<li class="brown dashboard_icon">
								<a href="home.php?page=reports">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Income Statement
									</span>
								</a>
							</li>
							<li class="blue dashboard_icon">
								<a href="home.php?page=reports">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Balance Sheet
									</span>
								</a>
							</li>
							<li class="lightgrey dashboard_icon">
								<a href="home.php?page=reports">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Final Account
									</span>
								</a>
							</li>
							<li class="satblue dashboard_icon">
								<a href="home.php?page=reports">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Shares Investment
									</span>
								</a>
							</li>
							<li class="lime dashboard_icon">
								<a href="home.php?page=reports">
									<span>
										<i class="icon-question-sign"></i>
									</span>
									<span class="name" style="text-align:center">
										Asset
									</span>
								</a>
							</li>
						</ul>
						</div>
					</div>
				<!-- End of dash board metro icons -->
				<div class="row-fluid">
					<div class="span12">
						<div class="box box-color box-bordered lightgrey">
							<div class="box-title">
								<h3><i class="icon-ok"></i> Tasks</h3>
								<div class="actions">
									<a href="#new-task" data-toggle="modal" class="btn"><i class="icon-plus-sign"></i> Add Task</a>
								</div>
							</div>
							<div class="box-content nopadding">
								<ul class="tasklist ui-sortable">
									<li class="bookmarked">
										<div class="check">
											<div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" class="icheck-me" data-skin="square" data-color="blue" style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
										</div>
										<span class="task"><i class="icon-ok"></i><span>Approve new users</span></span>
										<span class="task-actions">
											<a href="#" class="task-delete" rel="tooltip" title="" data-original-title="Delete that task"><i class="icon-remove"></i></a>
											<a href="#" class="task-bookmark" rel="tooltip" title="" data-original-title="Mark as important"><i class="icon-bookmark-empty"></i></a>
										</span>
									</li>
									<li>
										<div class="check">
											<div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" class="icheck-me" data-skin="square" data-color="blue" style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
										</div>
										<span class="task"><i class="icon-bar-chart"></i><span>Check statistics</span></span>
										<span class="task-actions">
											<a href="#" class="task-delete" rel="tooltip" title="" data-original-title="Delete that task"><i class="icon-remove"></i></a>
											<a href="#" class="task-bookmark" rel="tooltip" title="" data-original-title="Mark as important"><i class="icon-bookmark-empty"></i></a>
										</span>
									</li>
									<li class="done">
										<div class="check">
											<div class="icheckbox_square-blue checked" style="position: relative;"><input type="checkbox" class="icheck-me" data-skin="square" data-color="blue" checked="" style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
										</div>
										<span class="task"><i class="icon-envelope"></i><span>Check for new mails</span></span>
										<span class="task-actions">
											<a href="#" class="task-delete" rel="tooltip" title="" data-original-title="Delete that task"><i class="icon-remove"></i></a>
											<a href="#" class="task-bookmark" rel="tooltip" title="" data-original-title="Mark as important"><i class="icon-bookmark-empty"></i></a>
										</span>
									</li>
									<li>
										<div class="check">
											<div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" class="icheck-me" data-skin="square" data-color="blue" style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
										</div>
										<span class="task"><i class="icon-comment"></i><span>Chat with John Doe</span></span>
										<span class="task-actions">
											<a href="#" class="task-delete" rel="tooltip" title="" data-original-title="Delete that task"><i class="icon-remove"></i></a>
											<a href="#" class="task-bookmark" rel="tooltip" title="" data-original-title="Mark as important"><i class="icon-bookmark-empty"></i></a>
										</span>
									</li>
									<li>
										<div class="check">
											<div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" class="icheck-me" data-skin="square" data-color="blue" style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
										</div>
										<span class="task"><i class="icon-retweet"></i><span>Go and tweet some stuff</span></span>
										<span class="task-actions">
											<a href="#" class="task-delete" rel="tooltip" title="" data-original-title="Delete that task"><i class="icon-remove"></i></a>
											<a href="#" class="task-bookmark" rel="tooltip" title="" data-original-title="Mark as important"><i class="icon-bookmark-empty"></i></a>
										</span>
									</li>
									<li>
										<div class="check">
											<div class="icheckbox_square-blue" style="position: relative;"><input type="checkbox" class="icheck-me" data-skin="square" data-color="blue" style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"><ins style="position: absolute; top: -10%; left: -10%; display: block; width: 120%; height: 120%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
										</div>
										<span class="task"><i class="icon-edit"></i><span>Write an article</span></span>
										<span class="task-actions">
											<a href="#" class="task-delete" rel="tooltip" title="" data-original-title="Delete that task"><i class="icon-remove"></i></a>
											<a href="#" class="task-bookmark" rel="tooltip" title="" data-original-title="Mark as important"><i class="icon-bookmark-empty"></i></a>
										</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				<div class="page-header">
					<div class="pull-left">
						<h1>&nbsp;</h1>
					</div>
					<div class="pull-right">
						
						<ul class="stats">
							<li class="lightred">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">June 1, 2014</span>
									<span>Sunday, 9:48</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				
			</div>