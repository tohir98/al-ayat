<!-- Page Header -->
<div class="page-header">
    <div class="pull-left">
        <h3><a href="home.php?page=members" class="btn btn-warning"><i class="icon-arrow-left"></i> Back </a></h3>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        
    </div>
</div>

<div class="breadcrumbs">
	<ul>
		<li>
			<a href="home.php?page=members">Members</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="#">New Member Registration</a>
			
		</li>
	</ul>
</div>
<!-- end of breacrumb -->
<div class="row-fluid">
	<div class="span12">
		<div class="box">
			<div class="box-title">
				<h3>
					<i class="icon-ok"></i>
					Membership Registration
				</h3>
			</div>
			<div class="box-content">
				<form action="#" method="POST" class='form-horizontal form-validate' id="aaa">
					<div class="control-group">
						<label for="textfield" class="control-label">Select</label>
						<div class="controls">
							<select name="aaa" id="bbb" data-rule-required="true">
								<option value="">-- Please select --</option>
								<option value="1">Option-1</option>
								<option value="2">Option-2</option>
								<option value="3">Option-3</option>
								<option value="4">Option-4</option>
								<option value="5">Option-5</option>
								<option value="6">Option-6</option>
								<option value="7">Option-7</option>
								<option value="8">Option-8</option>
								<option value="9">Option-9</option>
								<option value="10">Option-10</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="urlfield" class="control-label">URL <small>with http://</small></label>
						<div class="controls">
							<input type="text" placeholder="Enter valid URL" name="urlfield" id="urlfield" data-rule-url="true" data-rule-required="true">
						</div>
					</div>
					<div class="control-group">
						<label for="minlengthfield" class="control-label">Minlength <small>minlength: 3</small></label>
						<div class="controls">
							<input type="text" placeholder="At least 3 characters" name="minlengthfield" id="minlengthfield" data-rule-minlength="3" data-rule-required="true">
						</div>
					</div>
					<div class="control-group">
						<label for="maxlengthfield" class="control-label">Maxlength <small>maxlength: 6</small></label>
						<div class="controls">
							<input type="text" placeholder="At least 3 characters" name="maxlengthfield" id="maxlengthfield" data-rule-maxlength="6" data-rule-required="true">
						</div>
					</div>
					<div class="control-group">
						<label for="datefield" class="control-label">Date <small>YYYY-MM-DD</small></label>
						<div class="controls">
							<input type="text" placeholder="Only numbers" name="datefield" id="datefield" data-rule-dateISO="true" data-rule-required="true">
						</div>
					</div>
					<div class="control-group">
						<label for="numberfield" class="control-label">Number</label>
						<div class="controls">
							<input type="text" placeholder="Only numbers" name="numberfield" id="numberfield" data-rule-number="true" data-rule-required="true">
						</div>
					</div>
					<div class="control-group">
						<label for="digitsfield" class="control-label">Digits</label>
						<div class="controls">
							<input type="text" placeholder="Only digits" name="digitsfield" id="digitsfield" data-rule-digits="true" data-rule-required="true">
						</div>
					</div>
					<div class="control-group">
						<label for="creditcardfield" class="control-label">Creditcard <small>try 446-667-651</small></label>
						<div class="controls">
							<input type="text" placeholder="Enter valid creditcard" name="creditcardfield" id="creditcardfield" data-rule-creditcard="true" data-rule-required="true">
						</div>
					</div>
					<div class="form-actions">
						<input type="submit" class="btn btn-primary" value="Submit">
						<button type="button" class="btn">Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>