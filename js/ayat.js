//
$(document).ready(function() {
	
	$('.change_task_status').click(function() {
		
		status = $(this).attr('data-status');
		id_task = $(this).attr('data-id_task');
		
		$('#change_task_status_dialog').modal();
		
		return false;
	});
	
	$('#change_task_status_button').click(function() {
		
		$('#change_task_status_dialog').modal('hide');
		
		location.href = BASE_URL + 'tasks/change_status/'+id_task+'/'+status+'/dashboard';
	});
	
	/************** Loader *****************/
	
	$('a').click(function() {
		
		var href = $(this).attr('href');
		var blank = $(this).attr('target');
		
		if(typeof href != 'undefined') {
			return true;
		}
		
		if(typeof href != 'undefined') {
		if(href != '') {
			var fs = href.substring(0,4);
			if(fs == 'http') {
				show_loader();
			}
		}
		}
	});
	
	hide_loader();
	
});

	function show_loader() {
		$('body').addClass('body_op');
		$('#loader').show();
	}	
	
	function hide_loader() {
		$('body').removeClass('body_op');
		$('#loader').hide();
	}
	
function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
	function getLga(stateId) {		
		
		var strURL="findLga.php?state_id="+stateId;
		var req = getXMLHTTP();
		
		if (req) {
			
			document.getElementById('lgadiv').innerHTML="Loading... pls wait";
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('lgadiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}		
	}
	
	function getCity(countryId,stateId) {		
		var strURL="findCity.php?country="+countryId+"&state="+stateId;
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('citydiv').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
				
	}