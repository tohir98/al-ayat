<?php
if ($_SESSION['logged'] != true) {
    $msg = base64_encode("Welcome, Please Login!...");
    header("Location: ../login.php?msg=$msg");
}
?>

<?php include './_notification.php'; ?>

<div class="page-header">
    <div class="pull-left">
        <h1>Receipt & Payments</h1>
        <a class="btn btn-warning" href="home.php?page=dashboard"><i class="icon-arrow-left"></i>&nbsp;Back</a>
    </div>
</div>

<br />

<!--  Breadcrumbs  -->
<div class="breadcrumbs">
    <ul>
        <li>
            <a href="home.php?page=dashboard">Dashboard</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="home.php">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Receipt &amp; Payments</a>
        </li>
    </ul>
</div>

<div class="box box-bordered ">
    <div class="box-title">
        <select name="cboYear" id="cboYear" class="select2-me input-mini">
            <option></option>
            <?php for ($year = date('Y'); $year >= 2014; --$year): ?>
                <option value="<?= $year ?>"><?= $year ?></option>
            <?php endfor; ?>
        </select>
    </div>
    <div class="box-content nopadding">


        <table class="table table-user table-hover table-nomargin dataTable">
            <thead>
                <tr>
                    <th>Financial Sec</th>
                    <th>Total</th>
                    <th class='hidden-1024'>Month</th>
                    <th class='hidden-480'>Year</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $Rqry = ExecuteSQLQuery("SELECT created_by,  `year`, `month`, sum(total) as total, r.Firstname, r.Lastname "
                    . "FROM tbl_financial_sheet fs "
                    . "INNER JOIN tbl_role r ON r.role_id = fs.created_by "
                    . "GROUP BY `month`, `year`, created_by");
                $counter = 1;
                while ($rowRqry = mysqli_fetch_array($Rqry)) :
                    ?>
                    <tr>
                        <td><?= ucfirst($rowRqry["Firstname"]) . ' ' . ucfirst($rowRqry["Lastname"]); ?> </td>
                        <td>NGN <?= number_format($rowRqry["total"], 2); ?> </td>
                        <td><?= $months[$rowRqry["month"]]; ?></td>
                        <td><?= $rowRqry["year"]; ?></td>
                    </tr>
                    <?php
                    $counter++;
                endwhile;
                ?>
            </tbody>
        </table>
    </div>    
</div>


<!-- Remove user Modal -->
<div id="modal-2" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-body">
        <p>Are you sure you want to delete this record?</p>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
        <a href="#" class="btn btn-danger closeme">Yes, delete</a>
    </div>
</div>
<!-- End Remove Shift Modal -->


<!-- Edit user dialog -->
<div id="edit_record_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<!-- View user dialog -->
<div id="view_record_modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
</div>

<script type="text/javascript">

    $('.modal_confirm').click(function (eve) {

        eve.preventDefault();
        $('#modal-2').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });



    $('.modal_confirm_shift').click(function (eve) {

        eve.preventDefault();
        $('#modal-3').modal('show').fadeIn();

        var page = $(this).attr("href");
        $('a.closeme').attr('href', page);

    });


    $('.ajaxlink_view').click(function (eve) {

        eve.preventDefault();
        $('#view_record_modal').modal('show');
        $('#view_record_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#view_record_modal').html('');
            $('#view_record_modal').html(html).show();


        });

    });


    $('.ajaxlink_edit').click(function (eve) {

        eve.preventDefault();
        $('#edit_record_modal').modal('show');
        $('#edit_record_modal').html('<div class="loaderBox"><img src="img/gif-load.gif" ></div>');

        var page = $(this).attr("href");
        $.get(page, function (html) {

            $('#edit_record_modal').html('');
            $('#edit_record_modal').html(html).show();


        });

    });


</script>